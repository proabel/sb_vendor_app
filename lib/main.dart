import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:sb_vendor/injection.dart';
import 'package:sb_vendor/presentation/app_widget.dart';

void main() {
  configureInjection(Environment.prod);
  runApp(AppWidget());
}
