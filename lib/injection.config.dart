// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'infrastructure/auth/auth_repository.dart';
import 'infrastructure/core/firebase_injectable_module.dart';
import 'domain/auth/i_auth_repository.dart';
import 'domain/selection/i_selection_repository.dart';
import 'domain/vendor_product/i_vendor_product_repository.dart';
import 'application/selection/selection_bloc.dart';
import 'infrastructure/selection/selection_repository.dart';
import 'application/auth/sign_in_form/sign_in_form_bloc.dart';
import 'infrastructure/vendor_product/vendor_product_repository.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final firebaseInjectableModule = _$FirebaseInjectableModule();
  gh.lazySingleton<Firestore>(() => firebaseInjectableModule.firestore);
  gh.lazySingleton<IAuthRepository>(() => AuthRepository());
  gh.lazySingleton<ISelectionRepository>(
      () => SelectionRepository(get<FirebaseFirestore>()));
  gh.lazySingleton<IVendorProductRepository>(
      () => VendorProductRepository(get<Firestore>()));
  gh.factory<SelectionBloc>(() => SelectionBloc(get<ISelectionRepository>()));
  gh.factory<SignInFormBloc>(() => SignInFormBloc(get<IAuthRepository>()));
  return get;
}

class _$FirebaseInjectableModule extends FirebaseInjectableModule {}
