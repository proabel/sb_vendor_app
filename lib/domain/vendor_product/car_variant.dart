import 'package:freezed_annotation/freezed_annotation.dart';

part 'car_variant.freezed.dart';
part 'car_variant.g.dart';

@freezed
abstract class CarVariant with _$CarVariant {
  const factory CarVariant({
    String carVariantID,
    @required String name,
    @required String series,
    @required List<String> years,
    String other,
    String engineType,
    String displacement,
    @required String manufacturerID,
    @required String carModelID,
  }) = _CarVariant;

  factory CarVariant.fromJson(Map<String, dynamic> json) => _$CarVariantFromJson(json);
}
