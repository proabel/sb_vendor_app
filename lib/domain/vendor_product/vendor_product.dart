import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sb_vendor/domain/vendor_product/car_variant.dart';

part 'vendor_product.freezed.dart';
part 'vendor_product.g.dart';

@freezed
abstract class VendorProduct with _$VendorProduct {
  const factory VendorProduct({
    @required String userID,
    @required String name,
    @required String description,
    @required int price,
    @required List<String> images,
    @required bool isAvailable,
    @required bool isApproved,
    @required bool isVisible,
    @required String manufacturerID,
    @required List<String> carModels,
    @required List<CarVariant> carVariants,
    @required String categoryID,
  }) = _VendorProduct;

  factory VendorProduct.fromJson(Map<String, dynamic> json) => _$VendorProductFromJson(json);
}