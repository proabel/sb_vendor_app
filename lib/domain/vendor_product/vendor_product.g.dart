// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vendor_product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_VendorProduct _$_$_VendorProductFromJson(Map<String, dynamic> json) {
  return _$_VendorProduct(
    userID: json['userID'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    price: json['price'] as int,
    images: (json['images'] as List)?.map((e) => e as String)?.toList(),
    isAvailable: json['isAvailable'] as bool,
    isApproved: json['isApproved'] as bool,
    isVisible: json['isVisible'] as bool,
    manufacturerID: json['manufacturerID'] as String,
    carModels: (json['carModels'] as List)?.map((e) => e as String)?.toList(),
    carVariants: (json['carVariants'] as List)
        ?.map((e) =>
            e == null ? null : CarVariant.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    categoryID: json['categoryID'] as String,
  );
}

Map<String, dynamic> _$_$_VendorProductToJson(_$_VendorProduct instance) =>
    <String, dynamic>{
      'userID': instance.userID,
      'name': instance.name,
      'description': instance.description,
      'price': instance.price,
      'images': instance.images,
      'isAvailable': instance.isAvailable,
      'isApproved': instance.isApproved,
      'isVisible': instance.isVisible,
      'manufacturerID': instance.manufacturerID,
      'carModels': instance.carModels,
      'carVariants': instance.carVariants,
      'categoryID': instance.categoryID,
    };
