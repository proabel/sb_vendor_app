// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car_variant.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CarVariant _$_$_CarVariantFromJson(Map<String, dynamic> json) {
  return _$_CarVariant(
    carVariantID: json['carVariantID'] as String,
    name: json['name'] as String,
    series: json['series'] as String,
    years: (json['years'] as List)?.map((e) => e as String)?.toList(),
    other: json['other'] as String,
    engineType: json['engineType'] as String,
    displacement: json['displacement'] as String,
    manufacturerID: json['manufacturerID'] as String,
    carModelID: json['carModelID'] as String,
  );
}

Map<String, dynamic> _$_$_CarVariantToJson(_$_CarVariant instance) =>
    <String, dynamic>{
      'carVariantID': instance.carVariantID,
      'name': instance.name,
      'series': instance.series,
      'years': instance.years,
      'other': instance.other,
      'engineType': instance.engineType,
      'displacement': instance.displacement,
      'manufacturerID': instance.manufacturerID,
      'carModelID': instance.carModelID,
    };
