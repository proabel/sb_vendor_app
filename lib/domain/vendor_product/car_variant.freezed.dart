// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'car_variant.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
CarVariant _$CarVariantFromJson(Map<String, dynamic> json) {
  return _CarVariant.fromJson(json);
}

class _$CarVariantTearOff {
  const _$CarVariantTearOff();

// ignore: unused_element
  _CarVariant call(
      {String carVariantID,
      @required String name,
      @required String series,
      @required List<String> years,
      String other,
      String engineType,
      String displacement,
      @required String manufacturerID,
      @required String carModelID}) {
    return _CarVariant(
      carVariantID: carVariantID,
      name: name,
      series: series,
      years: years,
      other: other,
      engineType: engineType,
      displacement: displacement,
      manufacturerID: manufacturerID,
      carModelID: carModelID,
    );
  }
}

// ignore: unused_element
const $CarVariant = _$CarVariantTearOff();

mixin _$CarVariant {
  String get carVariantID;
  String get name;
  String get series;
  List<String> get years;
  String get other;
  String get engineType;
  String get displacement;
  String get manufacturerID;
  String get carModelID;

  Map<String, dynamic> toJson();
  $CarVariantCopyWith<CarVariant> get copyWith;
}

abstract class $CarVariantCopyWith<$Res> {
  factory $CarVariantCopyWith(
          CarVariant value, $Res Function(CarVariant) then) =
      _$CarVariantCopyWithImpl<$Res>;
  $Res call(
      {String carVariantID,
      String name,
      String series,
      List<String> years,
      String other,
      String engineType,
      String displacement,
      String manufacturerID,
      String carModelID});
}

class _$CarVariantCopyWithImpl<$Res> implements $CarVariantCopyWith<$Res> {
  _$CarVariantCopyWithImpl(this._value, this._then);

  final CarVariant _value;
  // ignore: unused_field
  final $Res Function(CarVariant) _then;

  @override
  $Res call({
    Object carVariantID = freezed,
    Object name = freezed,
    Object series = freezed,
    Object years = freezed,
    Object other = freezed,
    Object engineType = freezed,
    Object displacement = freezed,
    Object manufacturerID = freezed,
    Object carModelID = freezed,
  }) {
    return _then(_value.copyWith(
      carVariantID: carVariantID == freezed
          ? _value.carVariantID
          : carVariantID as String,
      name: name == freezed ? _value.name : name as String,
      series: series == freezed ? _value.series : series as String,
      years: years == freezed ? _value.years : years as List<String>,
      other: other == freezed ? _value.other : other as String,
      engineType:
          engineType == freezed ? _value.engineType : engineType as String,
      displacement: displacement == freezed
          ? _value.displacement
          : displacement as String,
      manufacturerID: manufacturerID == freezed
          ? _value.manufacturerID
          : manufacturerID as String,
      carModelID:
          carModelID == freezed ? _value.carModelID : carModelID as String,
    ));
  }
}

abstract class _$CarVariantCopyWith<$Res> implements $CarVariantCopyWith<$Res> {
  factory _$CarVariantCopyWith(
          _CarVariant value, $Res Function(_CarVariant) then) =
      __$CarVariantCopyWithImpl<$Res>;
  @override
  $Res call(
      {String carVariantID,
      String name,
      String series,
      List<String> years,
      String other,
      String engineType,
      String displacement,
      String manufacturerID,
      String carModelID});
}

class __$CarVariantCopyWithImpl<$Res> extends _$CarVariantCopyWithImpl<$Res>
    implements _$CarVariantCopyWith<$Res> {
  __$CarVariantCopyWithImpl(
      _CarVariant _value, $Res Function(_CarVariant) _then)
      : super(_value, (v) => _then(v as _CarVariant));

  @override
  _CarVariant get _value => super._value as _CarVariant;

  @override
  $Res call({
    Object carVariantID = freezed,
    Object name = freezed,
    Object series = freezed,
    Object years = freezed,
    Object other = freezed,
    Object engineType = freezed,
    Object displacement = freezed,
    Object manufacturerID = freezed,
    Object carModelID = freezed,
  }) {
    return _then(_CarVariant(
      carVariantID: carVariantID == freezed
          ? _value.carVariantID
          : carVariantID as String,
      name: name == freezed ? _value.name : name as String,
      series: series == freezed ? _value.series : series as String,
      years: years == freezed ? _value.years : years as List<String>,
      other: other == freezed ? _value.other : other as String,
      engineType:
          engineType == freezed ? _value.engineType : engineType as String,
      displacement: displacement == freezed
          ? _value.displacement
          : displacement as String,
      manufacturerID: manufacturerID == freezed
          ? _value.manufacturerID
          : manufacturerID as String,
      carModelID:
          carModelID == freezed ? _value.carModelID : carModelID as String,
    ));
  }
}

@JsonSerializable()
class _$_CarVariant implements _CarVariant {
  const _$_CarVariant(
      {this.carVariantID,
      @required this.name,
      @required this.series,
      @required this.years,
      this.other,
      this.engineType,
      this.displacement,
      @required this.manufacturerID,
      @required this.carModelID})
      : assert(name != null),
        assert(series != null),
        assert(years != null),
        assert(manufacturerID != null),
        assert(carModelID != null);

  factory _$_CarVariant.fromJson(Map<String, dynamic> json) =>
      _$_$_CarVariantFromJson(json);

  @override
  final String carVariantID;
  @override
  final String name;
  @override
  final String series;
  @override
  final List<String> years;
  @override
  final String other;
  @override
  final String engineType;
  @override
  final String displacement;
  @override
  final String manufacturerID;
  @override
  final String carModelID;

  @override
  String toString() {
    return 'CarVariant(carVariantID: $carVariantID, name: $name, series: $series, years: $years, other: $other, engineType: $engineType, displacement: $displacement, manufacturerID: $manufacturerID, carModelID: $carModelID)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CarVariant &&
            (identical(other.carVariantID, carVariantID) ||
                const DeepCollectionEquality()
                    .equals(other.carVariantID, carVariantID)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.series, series) ||
                const DeepCollectionEquality().equals(other.series, series)) &&
            (identical(other.years, years) ||
                const DeepCollectionEquality().equals(other.years, years)) &&
            (identical(other.other, this.other) ||
                const DeepCollectionEquality()
                    .equals(other.other, this.other)) &&
            (identical(other.engineType, engineType) ||
                const DeepCollectionEquality()
                    .equals(other.engineType, engineType)) &&
            (identical(other.displacement, displacement) ||
                const DeepCollectionEquality()
                    .equals(other.displacement, displacement)) &&
            (identical(other.manufacturerID, manufacturerID) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturerID, manufacturerID)) &&
            (identical(other.carModelID, carModelID) ||
                const DeepCollectionEquality()
                    .equals(other.carModelID, carModelID)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(carVariantID) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(series) ^
      const DeepCollectionEquality().hash(years) ^
      const DeepCollectionEquality().hash(other) ^
      const DeepCollectionEquality().hash(engineType) ^
      const DeepCollectionEquality().hash(displacement) ^
      const DeepCollectionEquality().hash(manufacturerID) ^
      const DeepCollectionEquality().hash(carModelID);

  @override
  _$CarVariantCopyWith<_CarVariant> get copyWith =>
      __$CarVariantCopyWithImpl<_CarVariant>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CarVariantToJson(this);
  }
}

abstract class _CarVariant implements CarVariant {
  const factory _CarVariant(
      {String carVariantID,
      @required String name,
      @required String series,
      @required List<String> years,
      String other,
      String engineType,
      String displacement,
      @required String manufacturerID,
      @required String carModelID}) = _$_CarVariant;

  factory _CarVariant.fromJson(Map<String, dynamic> json) =
      _$_CarVariant.fromJson;

  @override
  String get carVariantID;
  @override
  String get name;
  @override
  String get series;
  @override
  List<String> get years;
  @override
  String get other;
  @override
  String get engineType;
  @override
  String get displacement;
  @override
  String get manufacturerID;
  @override
  String get carModelID;
  @override
  _$CarVariantCopyWith<_CarVariant> get copyWith;
}
