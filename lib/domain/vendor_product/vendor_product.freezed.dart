// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'vendor_product.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
VendorProduct _$VendorProductFromJson(Map<String, dynamic> json) {
  return _VendorProduct.fromJson(json);
}

class _$VendorProductTearOff {
  const _$VendorProductTearOff();

// ignore: unused_element
  _VendorProduct call(
      {@required String userID,
      @required String name,
      @required String description,
      @required int price,
      @required List<String> images,
      @required bool isAvailable,
      @required bool isApproved,
      @required bool isVisible,
      @required String manufacturerID,
      @required List<String> carModels,
      @required List<CarVariant> carVariants,
      @required String categoryID}) {
    return _VendorProduct(
      userID: userID,
      name: name,
      description: description,
      price: price,
      images: images,
      isAvailable: isAvailable,
      isApproved: isApproved,
      isVisible: isVisible,
      manufacturerID: manufacturerID,
      carModels: carModels,
      carVariants: carVariants,
      categoryID: categoryID,
    );
  }
}

// ignore: unused_element
const $VendorProduct = _$VendorProductTearOff();

mixin _$VendorProduct {
  String get userID;
  String get name;
  String get description;
  int get price;
  List<String> get images;
  bool get isAvailable;
  bool get isApproved;
  bool get isVisible;
  String get manufacturerID;
  List<String> get carModels;
  List<CarVariant> get carVariants;
  String get categoryID;

  Map<String, dynamic> toJson();
  $VendorProductCopyWith<VendorProduct> get copyWith;
}

abstract class $VendorProductCopyWith<$Res> {
  factory $VendorProductCopyWith(
          VendorProduct value, $Res Function(VendorProduct) then) =
      _$VendorProductCopyWithImpl<$Res>;
  $Res call(
      {String userID,
      String name,
      String description,
      int price,
      List<String> images,
      bool isAvailable,
      bool isApproved,
      bool isVisible,
      String manufacturerID,
      List<String> carModels,
      List<CarVariant> carVariants,
      String categoryID});
}

class _$VendorProductCopyWithImpl<$Res>
    implements $VendorProductCopyWith<$Res> {
  _$VendorProductCopyWithImpl(this._value, this._then);

  final VendorProduct _value;
  // ignore: unused_field
  final $Res Function(VendorProduct) _then;

  @override
  $Res call({
    Object userID = freezed,
    Object name = freezed,
    Object description = freezed,
    Object price = freezed,
    Object images = freezed,
    Object isAvailable = freezed,
    Object isApproved = freezed,
    Object isVisible = freezed,
    Object manufacturerID = freezed,
    Object carModels = freezed,
    Object carVariants = freezed,
    Object categoryID = freezed,
  }) {
    return _then(_value.copyWith(
      userID: userID == freezed ? _value.userID : userID as String,
      name: name == freezed ? _value.name : name as String,
      description:
          description == freezed ? _value.description : description as String,
      price: price == freezed ? _value.price : price as int,
      images: images == freezed ? _value.images : images as List<String>,
      isAvailable:
          isAvailable == freezed ? _value.isAvailable : isAvailable as bool,
      isApproved:
          isApproved == freezed ? _value.isApproved : isApproved as bool,
      isVisible: isVisible == freezed ? _value.isVisible : isVisible as bool,
      manufacturerID: manufacturerID == freezed
          ? _value.manufacturerID
          : manufacturerID as String,
      carModels:
          carModels == freezed ? _value.carModels : carModels as List<String>,
      carVariants: carVariants == freezed
          ? _value.carVariants
          : carVariants as List<CarVariant>,
      categoryID:
          categoryID == freezed ? _value.categoryID : categoryID as String,
    ));
  }
}

abstract class _$VendorProductCopyWith<$Res>
    implements $VendorProductCopyWith<$Res> {
  factory _$VendorProductCopyWith(
          _VendorProduct value, $Res Function(_VendorProduct) then) =
      __$VendorProductCopyWithImpl<$Res>;
  @override
  $Res call(
      {String userID,
      String name,
      String description,
      int price,
      List<String> images,
      bool isAvailable,
      bool isApproved,
      bool isVisible,
      String manufacturerID,
      List<String> carModels,
      List<CarVariant> carVariants,
      String categoryID});
}

class __$VendorProductCopyWithImpl<$Res>
    extends _$VendorProductCopyWithImpl<$Res>
    implements _$VendorProductCopyWith<$Res> {
  __$VendorProductCopyWithImpl(
      _VendorProduct _value, $Res Function(_VendorProduct) _then)
      : super(_value, (v) => _then(v as _VendorProduct));

  @override
  _VendorProduct get _value => super._value as _VendorProduct;

  @override
  $Res call({
    Object userID = freezed,
    Object name = freezed,
    Object description = freezed,
    Object price = freezed,
    Object images = freezed,
    Object isAvailable = freezed,
    Object isApproved = freezed,
    Object isVisible = freezed,
    Object manufacturerID = freezed,
    Object carModels = freezed,
    Object carVariants = freezed,
    Object categoryID = freezed,
  }) {
    return _then(_VendorProduct(
      userID: userID == freezed ? _value.userID : userID as String,
      name: name == freezed ? _value.name : name as String,
      description:
          description == freezed ? _value.description : description as String,
      price: price == freezed ? _value.price : price as int,
      images: images == freezed ? _value.images : images as List<String>,
      isAvailable:
          isAvailable == freezed ? _value.isAvailable : isAvailable as bool,
      isApproved:
          isApproved == freezed ? _value.isApproved : isApproved as bool,
      isVisible: isVisible == freezed ? _value.isVisible : isVisible as bool,
      manufacturerID: manufacturerID == freezed
          ? _value.manufacturerID
          : manufacturerID as String,
      carModels:
          carModels == freezed ? _value.carModels : carModels as List<String>,
      carVariants: carVariants == freezed
          ? _value.carVariants
          : carVariants as List<CarVariant>,
      categoryID:
          categoryID == freezed ? _value.categoryID : categoryID as String,
    ));
  }
}

@JsonSerializable()
class _$_VendorProduct implements _VendorProduct {
  const _$_VendorProduct(
      {@required this.userID,
      @required this.name,
      @required this.description,
      @required this.price,
      @required this.images,
      @required this.isAvailable,
      @required this.isApproved,
      @required this.isVisible,
      @required this.manufacturerID,
      @required this.carModels,
      @required this.carVariants,
      @required this.categoryID})
      : assert(userID != null),
        assert(name != null),
        assert(description != null),
        assert(price != null),
        assert(images != null),
        assert(isAvailable != null),
        assert(isApproved != null),
        assert(isVisible != null),
        assert(manufacturerID != null),
        assert(carModels != null),
        assert(carVariants != null),
        assert(categoryID != null);

  factory _$_VendorProduct.fromJson(Map<String, dynamic> json) =>
      _$_$_VendorProductFromJson(json);

  @override
  final String userID;
  @override
  final String name;
  @override
  final String description;
  @override
  final int price;
  @override
  final List<String> images;
  @override
  final bool isAvailable;
  @override
  final bool isApproved;
  @override
  final bool isVisible;
  @override
  final String manufacturerID;
  @override
  final List<String> carModels;
  @override
  final List<CarVariant> carVariants;
  @override
  final String categoryID;

  @override
  String toString() {
    return 'VendorProduct(userID: $userID, name: $name, description: $description, price: $price, images: $images, isAvailable: $isAvailable, isApproved: $isApproved, isVisible: $isVisible, manufacturerID: $manufacturerID, carModels: $carModels, carVariants: $carVariants, categoryID: $categoryID)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _VendorProduct &&
            (identical(other.userID, userID) ||
                const DeepCollectionEquality().equals(other.userID, userID)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.price, price) ||
                const DeepCollectionEquality().equals(other.price, price)) &&
            (identical(other.images, images) ||
                const DeepCollectionEquality().equals(other.images, images)) &&
            (identical(other.isAvailable, isAvailable) ||
                const DeepCollectionEquality()
                    .equals(other.isAvailable, isAvailable)) &&
            (identical(other.isApproved, isApproved) ||
                const DeepCollectionEquality()
                    .equals(other.isApproved, isApproved)) &&
            (identical(other.isVisible, isVisible) ||
                const DeepCollectionEquality()
                    .equals(other.isVisible, isVisible)) &&
            (identical(other.manufacturerID, manufacturerID) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturerID, manufacturerID)) &&
            (identical(other.carModels, carModels) ||
                const DeepCollectionEquality()
                    .equals(other.carModels, carModels)) &&
            (identical(other.carVariants, carVariants) ||
                const DeepCollectionEquality()
                    .equals(other.carVariants, carVariants)) &&
            (identical(other.categoryID, categoryID) ||
                const DeepCollectionEquality()
                    .equals(other.categoryID, categoryID)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userID) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(price) ^
      const DeepCollectionEquality().hash(images) ^
      const DeepCollectionEquality().hash(isAvailable) ^
      const DeepCollectionEquality().hash(isApproved) ^
      const DeepCollectionEquality().hash(isVisible) ^
      const DeepCollectionEquality().hash(manufacturerID) ^
      const DeepCollectionEquality().hash(carModels) ^
      const DeepCollectionEquality().hash(carVariants) ^
      const DeepCollectionEquality().hash(categoryID);

  @override
  _$VendorProductCopyWith<_VendorProduct> get copyWith =>
      __$VendorProductCopyWithImpl<_VendorProduct>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_VendorProductToJson(this);
  }
}

abstract class _VendorProduct implements VendorProduct {
  const factory _VendorProduct(
      {@required String userID,
      @required String name,
      @required String description,
      @required int price,
      @required List<String> images,
      @required bool isAvailable,
      @required bool isApproved,
      @required bool isVisible,
      @required String manufacturerID,
      @required List<String> carModels,
      @required List<CarVariant> carVariants,
      @required String categoryID}) = _$_VendorProduct;

  factory _VendorProduct.fromJson(Map<String, dynamic> json) =
      _$_VendorProduct.fromJson;

  @override
  String get userID;
  @override
  String get name;
  @override
  String get description;
  @override
  int get price;
  @override
  List<String> get images;
  @override
  bool get isAvailable;
  @override
  bool get isApproved;
  @override
  bool get isVisible;
  @override
  String get manufacturerID;
  @override
  List<String> get carModels;
  @override
  List<CarVariant> get carVariants;
  @override
  String get categoryID;
  @override
  _$VendorProductCopyWith<_VendorProduct> get copyWith;
}
