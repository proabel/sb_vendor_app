import 'package:sb_vendor/domain/vendor_product/vendor_product.dart';

abstract class IVendorProductRepository {
  Future createVendorProduct(VendorProduct vendorProduct);
}