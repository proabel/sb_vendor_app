import 'package:meta/meta.dart';
abstract class IAuthRepository {
  Future getSignedInUser();
  Future signInWithEmailAndPassword({
    @required String emailAddress, 
    @required String password
  });
  Future registerWithEmailAndPassword({
    @required String fullName,
    @required String emailAddress,
    @required String phoneNumber,
    @required String address,
    @required String businessName
  });
  Future signOut();
}