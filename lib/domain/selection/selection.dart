import 'package:freezed_annotation/freezed_annotation.dart';

part 'selection.freezed.dart';

@freezed
abstract class Selection with _$Selection {
  const factory Selection({
    @required String manufacturerID,
    @required String manufacturer,
    @required String carModelID,
    @required String carModel,
    @required String year,
    @required String carVariantID,
    @required String carVariant,
    @required String categoryID,
    @required String category
  }) = _Selection;

  factory Selection.empty() => Selection(
    manufacturerID: "",
    manufacturer: "",
    carModelID: "",
    carModel: "",
    year: "2000",
    carVariantID: "",
    carVariant: "",
    categoryID: "",
    category: "",
  );
}