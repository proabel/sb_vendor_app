import 'package:sb_vendor/domain/vendor_product/car_variant.dart';

abstract class ISelectionRepository{
  Future<List<Map<String, dynamic>>> getManufacturers();
  Future<List<Map<String, dynamic>>> getCarModels(String manufacturerID);
  Future<List<CarVariant>> getVariants(String year, String carModelID);
  Future<List<Map<String, dynamic>>> getCategories(); 
}