// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'selection.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SelectionTearOff {
  const _$SelectionTearOff();

// ignore: unused_element
  _Selection call(
      {@required String manufacturerID,
      @required String manufacturer,
      @required String carModelID,
      @required String carModel,
      @required String year,
      @required String carVariantID,
      @required String carVariant,
      @required String categoryID,
      @required String category}) {
    return _Selection(
      manufacturerID: manufacturerID,
      manufacturer: manufacturer,
      carModelID: carModelID,
      carModel: carModel,
      year: year,
      carVariantID: carVariantID,
      carVariant: carVariant,
      categoryID: categoryID,
      category: category,
    );
  }
}

// ignore: unused_element
const $Selection = _$SelectionTearOff();

mixin _$Selection {
  String get manufacturerID;
  String get manufacturer;
  String get carModelID;
  String get carModel;
  String get year;
  String get carVariantID;
  String get carVariant;
  String get categoryID;
  String get category;

  $SelectionCopyWith<Selection> get copyWith;
}

abstract class $SelectionCopyWith<$Res> {
  factory $SelectionCopyWith(Selection value, $Res Function(Selection) then) =
      _$SelectionCopyWithImpl<$Res>;
  $Res call(
      {String manufacturerID,
      String manufacturer,
      String carModelID,
      String carModel,
      String year,
      String carVariantID,
      String carVariant,
      String categoryID,
      String category});
}

class _$SelectionCopyWithImpl<$Res> implements $SelectionCopyWith<$Res> {
  _$SelectionCopyWithImpl(this._value, this._then);

  final Selection _value;
  // ignore: unused_field
  final $Res Function(Selection) _then;

  @override
  $Res call({
    Object manufacturerID = freezed,
    Object manufacturer = freezed,
    Object carModelID = freezed,
    Object carModel = freezed,
    Object year = freezed,
    Object carVariantID = freezed,
    Object carVariant = freezed,
    Object categoryID = freezed,
    Object category = freezed,
  }) {
    return _then(_value.copyWith(
      manufacturerID: manufacturerID == freezed
          ? _value.manufacturerID
          : manufacturerID as String,
      manufacturer: manufacturer == freezed
          ? _value.manufacturer
          : manufacturer as String,
      carModelID:
          carModelID == freezed ? _value.carModelID : carModelID as String,
      carModel: carModel == freezed ? _value.carModel : carModel as String,
      year: year == freezed ? _value.year : year as String,
      carVariantID: carVariantID == freezed
          ? _value.carVariantID
          : carVariantID as String,
      carVariant:
          carVariant == freezed ? _value.carVariant : carVariant as String,
      categoryID:
          categoryID == freezed ? _value.categoryID : categoryID as String,
      category: category == freezed ? _value.category : category as String,
    ));
  }
}

abstract class _$SelectionCopyWith<$Res> implements $SelectionCopyWith<$Res> {
  factory _$SelectionCopyWith(
          _Selection value, $Res Function(_Selection) then) =
      __$SelectionCopyWithImpl<$Res>;
  @override
  $Res call(
      {String manufacturerID,
      String manufacturer,
      String carModelID,
      String carModel,
      String year,
      String carVariantID,
      String carVariant,
      String categoryID,
      String category});
}

class __$SelectionCopyWithImpl<$Res> extends _$SelectionCopyWithImpl<$Res>
    implements _$SelectionCopyWith<$Res> {
  __$SelectionCopyWithImpl(_Selection _value, $Res Function(_Selection) _then)
      : super(_value, (v) => _then(v as _Selection));

  @override
  _Selection get _value => super._value as _Selection;

  @override
  $Res call({
    Object manufacturerID = freezed,
    Object manufacturer = freezed,
    Object carModelID = freezed,
    Object carModel = freezed,
    Object year = freezed,
    Object carVariantID = freezed,
    Object carVariant = freezed,
    Object categoryID = freezed,
    Object category = freezed,
  }) {
    return _then(_Selection(
      manufacturerID: manufacturerID == freezed
          ? _value.manufacturerID
          : manufacturerID as String,
      manufacturer: manufacturer == freezed
          ? _value.manufacturer
          : manufacturer as String,
      carModelID:
          carModelID == freezed ? _value.carModelID : carModelID as String,
      carModel: carModel == freezed ? _value.carModel : carModel as String,
      year: year == freezed ? _value.year : year as String,
      carVariantID: carVariantID == freezed
          ? _value.carVariantID
          : carVariantID as String,
      carVariant:
          carVariant == freezed ? _value.carVariant : carVariant as String,
      categoryID:
          categoryID == freezed ? _value.categoryID : categoryID as String,
      category: category == freezed ? _value.category : category as String,
    ));
  }
}

class _$_Selection implements _Selection {
  const _$_Selection(
      {@required this.manufacturerID,
      @required this.manufacturer,
      @required this.carModelID,
      @required this.carModel,
      @required this.year,
      @required this.carVariantID,
      @required this.carVariant,
      @required this.categoryID,
      @required this.category})
      : assert(manufacturerID != null),
        assert(manufacturer != null),
        assert(carModelID != null),
        assert(carModel != null),
        assert(year != null),
        assert(carVariantID != null),
        assert(carVariant != null),
        assert(categoryID != null),
        assert(category != null);

  @override
  final String manufacturerID;
  @override
  final String manufacturer;
  @override
  final String carModelID;
  @override
  final String carModel;
  @override
  final String year;
  @override
  final String carVariantID;
  @override
  final String carVariant;
  @override
  final String categoryID;
  @override
  final String category;

  @override
  String toString() {
    return 'Selection(manufacturerID: $manufacturerID, manufacturer: $manufacturer, carModelID: $carModelID, carModel: $carModel, year: $year, carVariantID: $carVariantID, carVariant: $carVariant, categoryID: $categoryID, category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Selection &&
            (identical(other.manufacturerID, manufacturerID) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturerID, manufacturerID)) &&
            (identical(other.manufacturer, manufacturer) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturer, manufacturer)) &&
            (identical(other.carModelID, carModelID) ||
                const DeepCollectionEquality()
                    .equals(other.carModelID, carModelID)) &&
            (identical(other.carModel, carModel) ||
                const DeepCollectionEquality()
                    .equals(other.carModel, carModel)) &&
            (identical(other.year, year) ||
                const DeepCollectionEquality().equals(other.year, year)) &&
            (identical(other.carVariantID, carVariantID) ||
                const DeepCollectionEquality()
                    .equals(other.carVariantID, carVariantID)) &&
            (identical(other.carVariant, carVariant) ||
                const DeepCollectionEquality()
                    .equals(other.carVariant, carVariant)) &&
            (identical(other.categoryID, categoryID) ||
                const DeepCollectionEquality()
                    .equals(other.categoryID, categoryID)) &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(manufacturerID) ^
      const DeepCollectionEquality().hash(manufacturer) ^
      const DeepCollectionEquality().hash(carModelID) ^
      const DeepCollectionEquality().hash(carModel) ^
      const DeepCollectionEquality().hash(year) ^
      const DeepCollectionEquality().hash(carVariantID) ^
      const DeepCollectionEquality().hash(carVariant) ^
      const DeepCollectionEquality().hash(categoryID) ^
      const DeepCollectionEquality().hash(category);

  @override
  _$SelectionCopyWith<_Selection> get copyWith =>
      __$SelectionCopyWithImpl<_Selection>(this, _$identity);
}

abstract class _Selection implements Selection {
  const factory _Selection(
      {@required String manufacturerID,
      @required String manufacturer,
      @required String carModelID,
      @required String carModel,
      @required String year,
      @required String carVariantID,
      @required String carVariant,
      @required String categoryID,
      @required String category}) = _$_Selection;

  @override
  String get manufacturerID;
  @override
  String get manufacturer;
  @override
  String get carModelID;
  @override
  String get carModel;
  @override
  String get year;
  @override
  String get carVariantID;
  @override
  String get carVariant;
  @override
  String get categoryID;
  @override
  String get category;
  @override
  _$SelectionCopyWith<_Selection> get copyWith;
}
