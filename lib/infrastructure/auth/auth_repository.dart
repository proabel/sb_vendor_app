import 'package:injectable/injectable.dart';
import 'package:sb_vendor/domain/auth/i_auth_repository.dart';

@LazySingleton(as: IAuthRepository)
class AuthRepository extends IAuthRepository{
  @override
  Future getSignedInUser() {
    // TODO: implement getSignedInUser
    throw UnimplementedError();
  }

  @override
  Future registerWithEmailAndPassword({String fullName, String emailAddress, String phoneNumber, String address, String businessName}) {
      // TODO: implement registerWithEmailAndPassword
      throw UnimplementedError();
    }
  
    @override
    Future signInWithEmailAndPassword({String emailAddress, String password}) {
    // TODO: implement signInWithEmailAndPassword
    throw UnimplementedError();
  }

  @override
  Future signOut() {
    // TODO: implement signOut
    throw UnimplementedError();
  }

}