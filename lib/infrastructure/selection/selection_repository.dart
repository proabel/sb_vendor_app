import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';
import 'package:sb_vendor/domain/selection/i_selection_repository.dart';
import 'package:sb_vendor/domain/vendor_product/car_variant.dart';

@LazySingleton(as: ISelectionRepository)
class SelectionRepository implements ISelectionRepository {
  final FirebaseFirestore _firestore;

  SelectionRepository(this._firestore);

  @override
  Future<List<Map<String, dynamic>>> getCarModels(String manufacturerID) async {
    //print('getting car models $manufacturerID');
    final carModels = await _firestore
        .collection('carmodels')
        .where('manufacturerID', isEqualTo: manufacturerID)
        .get()
        .then((snapshot) => snapshot.docs
            .map((doc) =>
                {'carModelID': doc.id, 'name': doc.data()['name']})
            .toList());
    //print('got car models  $carModels');
    return Future.value(carModels);
  }

  @override
  Future<List<Map<String, dynamic>>> getCategories() async{
    final categories = await _firestore
        .collection('categories')
        .get()
        .then((snapshot) => snapshot.docs.map((doc) {
              return {
                'categoryID': doc.id,
                'name': doc.data()['name']
              };
            }).toList());
    return Future.value(categories);
  }

  @override
  Future<List<Map<String, dynamic>>> getManufacturers() async {
    final manufacturers = await _firestore
        .collection('manufacturers')
        .get()
        .then((snapshot) => snapshot.docs.map((doc) {
              return {
                'manufacturerID': doc.id,
                'name': doc.data()['name']
              };
            }).toList());
    return Future.value(manufacturers);
  }

  @override
  Future<List<CarVariant>> getVariants(String year, String carModelID) async {
    print('getting variants for $year, $carModelID');
    final carVariants = await _firestore
        .collection('carVariants')
        .where('carModelID', isEqualTo: carModelID)
        .where('years', arrayContains: year.toString())
        .get()
        .then((snapshot) => snapshot.docs
            .map((doc){
              print('$doc');
              CarVariant  carVariant = CarVariant.fromJson(doc.data());
              return carVariant.copyWith(carVariantID: doc.id);
            })
            .toList());
    return Future.value(carVariants);
  }
}
