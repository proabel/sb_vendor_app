import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';
import 'package:sb_vendor/domain/vendor_product/i_vendor_product_repository.dart';
import 'package:sb_vendor/domain/vendor_product/vendor_product.dart';

@LazySingleton(as: IVendorProductRepository)
class VendorProductRepository extends IVendorProductRepository{
  final Firestore _firestore;

    VendorProductRepository(this._firestore);
  @override
  Future createVendorProduct(VendorProduct vendorProduct) {
      return _firestore.collection('vendorProducts').add(vendorProduct.toJson());
  }
  
}