import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:sb_vendor/presentation/routes/router.gr.dart';

class SplashPage extends StatefulWidget {

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    ExtendedNavigator.of(context).pushSelectionInitialPage();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text('This is an awesome app'),
    );
  }
}