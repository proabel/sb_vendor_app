import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sb_vendor/presentation/routes/router.gr.dart';

import '../../application/selection/selection_bloc.dart';

class SelectionCarModelPage extends StatelessWidget {
  //const SelectionPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          appBar: AppBar(
            title: Text("Select a car model"),
          ),
          body: _buildBody(context),

    );
  }

  Widget _buildBody(context){
    return Container(
      child: BlocBuilder<SelectionBloc, SelectionState>(
        builder: (context, selectionState){
        // //SelectionState.
        
          final carModels = selectionState.carModels;
          return ListView.builder(
            itemCount: carModels.length,
            itemBuilder: (context, index){
            return GestureDetector(
              onTap: (){
                context.bloc<SelectionBloc>()
                .add(SelectionEvent.carModelSelected(
                  carModels[index]['carModelID'],
                  carModels[index]['name']
                ));
                ExtendedNavigator.of(context).pushSelectionYearPage();
              },
              child: ListTile(title: Text(carModels[index]['name'])));
       
        
            });
           
      }));
  }
}