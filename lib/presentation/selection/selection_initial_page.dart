import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sb_vendor/injection.dart';
import 'package:sb_vendor/application/selection/selection_bloc.dart';
import 'package:sb_vendor/presentation/routes/router.gr.dart';

class SelectionInitialPage extends StatelessWidget {
  //const SelectionPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: Text("Select a manufacturer"),
          ),
          body: _buildBody(context),
       
    );
  }

  Widget _buildBody(context){
    return Container(
      child: BlocBuilder<SelectionBloc, SelectionState>(
        builder: (context, selectionState){
        // //SelectionState.
        
          final manufacturers = selectionState.manufacturers;
          return ListView.builder(
            itemCount: manufacturers.length,
            itemBuilder: (context, index){
            return GestureDetector(
              onTap: (){
                context.bloc<SelectionBloc>()
                  .add(SelectionEvent.manufacturerSelected(
                    manufacturers[index]['manufacturerID'],
                    manufacturers[index]['name'],
                  ));
                ExtendedNavigator.of(context).pushSelectionCarModelPage();
              },
              child: ListTile(title: Text(manufacturers[index]['name'])));
       
        
            });
           
      }));
  }
}