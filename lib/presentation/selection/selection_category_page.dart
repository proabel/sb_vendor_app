import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sb_vendor/presentation/routes/router.gr.dart';

import '../../application/selection/selection_bloc.dart';

class SelectionCategoryPage extends StatelessWidget {
  const SelectionCategoryPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: Text("Select a category"),
          ),
          body: _buildBody(context),
       
    );
  }

  Widget _buildBody(context){
    return BlocBuilder<SelectionBloc, SelectionState>(builder: (context, selectionState){
       
    final categories = selectionState.categories;
    return Container(
      child:  ListView.builder(
            itemCount: categories.length,
            itemBuilder: (context, index){
            return GestureDetector(
              onTap: (){
                context.bloc<SelectionBloc>()
                  .add(SelectionEvent.categorySelected(
                    categories[index]['categoryID'],
                    categories[index]['name'],
                  ));
                print(selectionState.selection);
                //BlocProvider.of<SelectionBloc>(context).add(SelectionEvent.yearSelected(years[index].toString(), selectionState.selection.carModelID));
                //ExtendedNavigator.of(context).pushNamed(Routes.addProductPage());
              },
              child: ListTile(title: Text(categories[index]['name'])));
       
        
            }
      ));
    });
   
  }            
           
      
}