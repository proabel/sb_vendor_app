import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sb_vendor/presentation/routes/router.gr.dart';

import '../../application/selection/selection_bloc.dart';

class SelectionYearPage extends StatelessWidget {
  const SelectionYearPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: Text("Select year"),
          ),
          body: _buildBody(context),
       
    );
  }

  Widget _buildBody(context){
    return BlocBuilder<SelectionBloc, SelectionState>(builder: (context, selectionState){
       final int yearsCount = new DateTime.now().year - 2000;
    final List<int> years = List<int>.generate(yearsCount, (i) => i + 2000);
    return Container(
      child:  ListView.builder(
            itemCount: years.length,
            itemBuilder: (context, index){
            return GestureDetector(
              onTap: (){
                context.bloc<SelectionBloc>()
                  .add(SelectionEvent.yearSelected(
                    years[index].toString(), 
                    selectionState.selection.carModelID,
                    selectionState.selection.carModel,
                  ));
                //BlocProvider.of<SelectionBloc>(context).add(SelectionEvent.yearSelected(years[index].toString(), selectionState.selection.carModelID));
                ExtendedNavigator.of(context).pushSelectionCarVariantPage();
              },
              child: ListTile(title: Text(years[index].toString())));
       
        
            }
      ));
    });
   
  }            
           
      
}