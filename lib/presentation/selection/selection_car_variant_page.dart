import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sb_vendor/presentation/routes/router.gr.dart';

import '../../application/selection/selection_bloc.dart';

class SelectionCarVariantPage extends StatelessWidget {
  //const SelectionPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          appBar: AppBar(
            title: Text("Select a variant"),
          ),
          body: _buildBody(context),

    );
  }

  Widget _buildBody(context){
    return Container(
      child: BlocBuilder<SelectionBloc, SelectionState>(
        builder: (context, selectionState){
        // //SelectionState.
        
          final carVariants = selectionState.carVariants;
          return ListView.builder(
            itemCount: carVariants.length,
            itemBuilder: (context, index){
            return GestureDetector(
              onTap: (){
                context.bloc<SelectionBloc>()
                  .add(SelectionEvent.carVariantSelected(
                    carVariants[index].carVariantID,
                    '${carVariants[index].series} ${carVariants[index].name}'
                  ));
                ExtendedNavigator.of(context).pushSelectionCategoryPage();
              },
              child: ListTile(title: Text('${carVariants[index].series} ${carVariants[index].name}' )));
       
        
            });
           
      }));
  }
}