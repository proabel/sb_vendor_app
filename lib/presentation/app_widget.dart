import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sb_vendor/injection.dart';
import 'package:sb_vendor/presentation/routes/router.gr.dart';

import 'package:sb_vendor/application/selection/selection_bloc.dart';


class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SelectionBloc>(
        create: (context) => getIt<SelectionBloc>()
          ..add(SelectionEvent.selectionStarted()),
        )
      ], 
      child: MaterialApp(
        title: 'Hunt Planning App',
        debugShowCheckedModeBanner: false,
        builder: ExtendedNavigator(router: Router()),
      )
    );
    // return  MaterialApp(
    //     title: 'Hunt Planning App',
    //     debugShowCheckedModeBanner: false,
    //     builder: ExtendedNavigator(router: Router()),
    // );
  }
}