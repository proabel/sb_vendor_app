// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../selection/selection_car_model_page.dart';
import '../selection/selection_car_variant_page.dart';
import '../selection/selection_category_page.dart';
import '../selection/selection_initial_page.dart';
import '../selection/selection_year_page.dart';
import '../splash/splash_page.dart';

class Routes {
  static const String splashPage = '/';
  static const String selectionInitialPage = '/selection-initial-page';
  static const String selectionCarModelPage = '/selection-car-model-page';
  static const String selectionYearPage = '/selection-year-page';
  static const String selectionCarVariantPage = '/selection-car-variant-page';
  static const String selectionCategoryPage = '/selection-category-page';
  static const all = <String>{
    splashPage,
    selectionInitialPage,
    selectionCarModelPage,
    selectionYearPage,
    selectionCarVariantPage,
    selectionCategoryPage,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splashPage, page: SplashPage),
    RouteDef(Routes.selectionInitialPage, page: SelectionInitialPage),
    RouteDef(Routes.selectionCarModelPage, page: SelectionCarModelPage),
    RouteDef(Routes.selectionYearPage, page: SelectionYearPage),
    RouteDef(Routes.selectionCarVariantPage, page: SelectionCarVariantPage),
    RouteDef(Routes.selectionCategoryPage, page: SelectionCategoryPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    SplashPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SplashPage(),
        settings: data,
      );
    },
    SelectionInitialPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SelectionInitialPage(),
        settings: data,
      );
    },
    SelectionCarModelPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SelectionCarModelPage(),
        settings: data,
      );
    },
    SelectionYearPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const SelectionYearPage(),
        settings: data,
      );
    },
    SelectionCarVariantPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SelectionCarVariantPage(),
        settings: data,
      );
    },
    SelectionCategoryPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const SelectionCategoryPage(),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension RouterExtendedNavigatorStateX on ExtendedNavigatorState {
  Future<dynamic> pushSplashPage() => push<dynamic>(Routes.splashPage);

  Future<dynamic> pushSelectionInitialPage() =>
      push<dynamic>(Routes.selectionInitialPage);

  Future<dynamic> pushSelectionCarModelPage() =>
      push<dynamic>(Routes.selectionCarModelPage);

  Future<dynamic> pushSelectionYearPage() =>
      push<dynamic>(Routes.selectionYearPage);

  Future<dynamic> pushSelectionCarVariantPage() =>
      push<dynamic>(Routes.selectionCarVariantPage);

  Future<dynamic> pushSelectionCategoryPage() =>
      push<dynamic>(Routes.selectionCategoryPage);
}
