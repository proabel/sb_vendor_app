import 'package:auto_route/auto_route_annotations.dart';
import 'package:sb_vendor/presentation/selection/selection_car_model_page.dart';
import 'package:sb_vendor/presentation/selection/selection_car_variant_page.dart';
import 'package:sb_vendor/presentation/selection/selection_category_page.dart';
import 'package:sb_vendor/presentation/selection/selection_initial_page.dart';
import 'package:sb_vendor/presentation/selection/selection_year_page.dart';
import 'package:sb_vendor/presentation/splash/splash_page.dart';


@MaterialAutoRouter(
  generateNavigationHelperExtension: true,
  routes: <AutoRoute>[
    MaterialRoute(page: SplashPage, initial: true),
    MaterialRoute(page: SelectionInitialPage),
    MaterialRoute(page: SelectionCarModelPage),
    MaterialRoute(page: SelectionYearPage),
    MaterialRoute(page: SelectionCarVariantPage),
    MaterialRoute(page: SelectionCategoryPage),
  ]
)
class $Router {} 