import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sb_vendor/application/bloc/vendor_product_form_bloc.dart';
import 'package:sb_vendor/domain/selection/selection.dart';
import 'package:sb_vendor/domain/vendor_product/vendor_product.dart';
import '../../application/selection/selection_bloc.dart';

class VendorProductFormPage extends StatelessWidget {
  const VendorProductFormPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Product')
      ),
      body: _buildLayout(context),
    );
  }

  Widget _buildLayout(context){
    Selection _selection;
    TextEditingController nameFieldCtlr = TextEditingController();
    TextEditingController descFieldCtlr = TextEditingController();
    TextEditingController priceFieldCtlr = TextEditingController();
    BlocListener<SelectionBloc, SelectionState>(listener: (context, state){
      _selection = state.selection;
    });
    return BlocBuilder<VendorProductFormBloc, VendorProductFormState>(builder: (context, vendorProductFormState){
      return  Container(
        child: Column(
          children: <Widget>[
            TextField(
              controller: nameFieldCtlr,
              decoration: InputDecoration(
                labelText:'Product Name'
              ),
            ),
            TextField(
              controller: descFieldCtlr,
              decoration: InputDecoration(
                labelText:'Product Name'
              ),
            ),
            TextField(
              controller: priceFieldCtlr,
              decoration: InputDecoration(
                labelText:'Product Name'
              ),
            ),
            FlatButton(onPressed: (){

            }, child: Text('Add Images')),
            RaisedButton(onPressed: (){
              print('got selection $_selection');
              final VendorProduct newVendorProduct = VendorProduct(
                name: nameFieldCtlr.text,
                description: descFieldCtlr.text,
                price: int.parse(priceFieldCtlr.text)
              );
            }, child: Text('Add'),
            )

          ],
        )
      );
    });
  }
}