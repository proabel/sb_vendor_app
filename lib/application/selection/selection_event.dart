part of 'selection_bloc.dart';


@freezed
abstract class SelectionEvent with _$SelectionEvent {
  const factory SelectionEvent.selectionStarted() = _SelectionStarted;
  const factory SelectionEvent.manufacturerSelected(
    String manufacturerID, String manufacturer
  ) = _ManufacturerSelected;
  const factory SelectionEvent.carModelSelected(
    String carModelID, String carModel
  ) = _CarModelSelected;
  const factory SelectionEvent.yearSelected(
    String year, String carModelID, String carModel
  ) = _YearSelected;
  const factory SelectionEvent.carVariantSelected(
    String carVariantID, String carVariant 
  ) = _CarVariantSelected;
  const factory SelectionEvent.categorySelected(
    String categoryID, String category
  ) = _CategorySelected;
}