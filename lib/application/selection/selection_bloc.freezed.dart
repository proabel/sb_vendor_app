// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'selection_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SelectionEventTearOff {
  const _$SelectionEventTearOff();

// ignore: unused_element
  _SelectionStarted selectionStarted() {
    return const _SelectionStarted();
  }

// ignore: unused_element
  _ManufacturerSelected manufacturerSelected(
      String manufacturerID, String manufacturer) {
    return _ManufacturerSelected(
      manufacturerID,
      manufacturer,
    );
  }

// ignore: unused_element
  _CarModelSelected carModelSelected(String carModelID, String carModel) {
    return _CarModelSelected(
      carModelID,
      carModel,
    );
  }

// ignore: unused_element
  _YearSelected yearSelected(String year, String carModelID, String carModel) {
    return _YearSelected(
      year,
      carModelID,
      carModel,
    );
  }

// ignore: unused_element
  _CarVariantSelected carVariantSelected(
      String carVariantID, String carVariant) {
    return _CarVariantSelected(
      carVariantID,
      carVariant,
    );
  }

// ignore: unused_element
  _CategorySelected categorySelected(String categoryID, String category) {
    return _CategorySelected(
      categoryID,
      category,
    );
  }
}

// ignore: unused_element
const $SelectionEvent = _$SelectionEventTearOff();

mixin _$SelectionEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectionStarted(),
    @required
        Result manufacturerSelected(String manufacturerID, String manufacturer),
    @required Result carModelSelected(String carModelID, String carModel),
    @required
        Result yearSelected(String year, String carModelID, String carModel),
    @required Result carVariantSelected(String carVariantID, String carVariant),
    @required Result categorySelected(String categoryID, String category),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectionStarted(),
    Result manufacturerSelected(String manufacturerID, String manufacturer),
    Result carModelSelected(String carModelID, String carModel),
    Result yearSelected(String year, String carModelID, String carModel),
    Result carVariantSelected(String carVariantID, String carVariant),
    Result categorySelected(String categoryID, String category),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectionStarted(_SelectionStarted value),
    @required Result manufacturerSelected(_ManufacturerSelected value),
    @required Result carModelSelected(_CarModelSelected value),
    @required Result yearSelected(_YearSelected value),
    @required Result carVariantSelected(_CarVariantSelected value),
    @required Result categorySelected(_CategorySelected value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectionStarted(_SelectionStarted value),
    Result manufacturerSelected(_ManufacturerSelected value),
    Result carModelSelected(_CarModelSelected value),
    Result yearSelected(_YearSelected value),
    Result carVariantSelected(_CarVariantSelected value),
    Result categorySelected(_CategorySelected value),
    @required Result orElse(),
  });
}

abstract class $SelectionEventCopyWith<$Res> {
  factory $SelectionEventCopyWith(
          SelectionEvent value, $Res Function(SelectionEvent) then) =
      _$SelectionEventCopyWithImpl<$Res>;
}

class _$SelectionEventCopyWithImpl<$Res>
    implements $SelectionEventCopyWith<$Res> {
  _$SelectionEventCopyWithImpl(this._value, this._then);

  final SelectionEvent _value;
  // ignore: unused_field
  final $Res Function(SelectionEvent) _then;
}

abstract class _$SelectionStartedCopyWith<$Res> {
  factory _$SelectionStartedCopyWith(
          _SelectionStarted value, $Res Function(_SelectionStarted) then) =
      __$SelectionStartedCopyWithImpl<$Res>;
}

class __$SelectionStartedCopyWithImpl<$Res>
    extends _$SelectionEventCopyWithImpl<$Res>
    implements _$SelectionStartedCopyWith<$Res> {
  __$SelectionStartedCopyWithImpl(
      _SelectionStarted _value, $Res Function(_SelectionStarted) _then)
      : super(_value, (v) => _then(v as _SelectionStarted));

  @override
  _SelectionStarted get _value => super._value as _SelectionStarted;
}

class _$_SelectionStarted implements _SelectionStarted {
  const _$_SelectionStarted();

  @override
  String toString() {
    return 'SelectionEvent.selectionStarted()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SelectionStarted);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectionStarted(),
    @required
        Result manufacturerSelected(String manufacturerID, String manufacturer),
    @required Result carModelSelected(String carModelID, String carModel),
    @required
        Result yearSelected(String year, String carModelID, String carModel),
    @required Result carVariantSelected(String carVariantID, String carVariant),
    @required Result categorySelected(String categoryID, String category),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return selectionStarted();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectionStarted(),
    Result manufacturerSelected(String manufacturerID, String manufacturer),
    Result carModelSelected(String carModelID, String carModel),
    Result yearSelected(String year, String carModelID, String carModel),
    Result carVariantSelected(String carVariantID, String carVariant),
    Result categorySelected(String categoryID, String category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectionStarted != null) {
      return selectionStarted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectionStarted(_SelectionStarted value),
    @required Result manufacturerSelected(_ManufacturerSelected value),
    @required Result carModelSelected(_CarModelSelected value),
    @required Result yearSelected(_YearSelected value),
    @required Result carVariantSelected(_CarVariantSelected value),
    @required Result categorySelected(_CategorySelected value),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return selectionStarted(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectionStarted(_SelectionStarted value),
    Result manufacturerSelected(_ManufacturerSelected value),
    Result carModelSelected(_CarModelSelected value),
    Result yearSelected(_YearSelected value),
    Result carVariantSelected(_CarVariantSelected value),
    Result categorySelected(_CategorySelected value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectionStarted != null) {
      return selectionStarted(this);
    }
    return orElse();
  }
}

abstract class _SelectionStarted implements SelectionEvent {
  const factory _SelectionStarted() = _$_SelectionStarted;
}

abstract class _$ManufacturerSelectedCopyWith<$Res> {
  factory _$ManufacturerSelectedCopyWith(_ManufacturerSelected value,
          $Res Function(_ManufacturerSelected) then) =
      __$ManufacturerSelectedCopyWithImpl<$Res>;
  $Res call({String manufacturerID, String manufacturer});
}

class __$ManufacturerSelectedCopyWithImpl<$Res>
    extends _$SelectionEventCopyWithImpl<$Res>
    implements _$ManufacturerSelectedCopyWith<$Res> {
  __$ManufacturerSelectedCopyWithImpl(
      _ManufacturerSelected _value, $Res Function(_ManufacturerSelected) _then)
      : super(_value, (v) => _then(v as _ManufacturerSelected));

  @override
  _ManufacturerSelected get _value => super._value as _ManufacturerSelected;

  @override
  $Res call({
    Object manufacturerID = freezed,
    Object manufacturer = freezed,
  }) {
    return _then(_ManufacturerSelected(
      manufacturerID == freezed
          ? _value.manufacturerID
          : manufacturerID as String,
      manufacturer == freezed ? _value.manufacturer : manufacturer as String,
    ));
  }
}

class _$_ManufacturerSelected implements _ManufacturerSelected {
  const _$_ManufacturerSelected(this.manufacturerID, this.manufacturer)
      : assert(manufacturerID != null),
        assert(manufacturer != null);

  @override
  final String manufacturerID;
  @override
  final String manufacturer;

  @override
  String toString() {
    return 'SelectionEvent.manufacturerSelected(manufacturerID: $manufacturerID, manufacturer: $manufacturer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ManufacturerSelected &&
            (identical(other.manufacturerID, manufacturerID) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturerID, manufacturerID)) &&
            (identical(other.manufacturer, manufacturer) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturer, manufacturer)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(manufacturerID) ^
      const DeepCollectionEquality().hash(manufacturer);

  @override
  _$ManufacturerSelectedCopyWith<_ManufacturerSelected> get copyWith =>
      __$ManufacturerSelectedCopyWithImpl<_ManufacturerSelected>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectionStarted(),
    @required
        Result manufacturerSelected(String manufacturerID, String manufacturer),
    @required Result carModelSelected(String carModelID, String carModel),
    @required
        Result yearSelected(String year, String carModelID, String carModel),
    @required Result carVariantSelected(String carVariantID, String carVariant),
    @required Result categorySelected(String categoryID, String category),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return manufacturerSelected(manufacturerID, manufacturer);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectionStarted(),
    Result manufacturerSelected(String manufacturerID, String manufacturer),
    Result carModelSelected(String carModelID, String carModel),
    Result yearSelected(String year, String carModelID, String carModel),
    Result carVariantSelected(String carVariantID, String carVariant),
    Result categorySelected(String categoryID, String category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (manufacturerSelected != null) {
      return manufacturerSelected(manufacturerID, manufacturer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectionStarted(_SelectionStarted value),
    @required Result manufacturerSelected(_ManufacturerSelected value),
    @required Result carModelSelected(_CarModelSelected value),
    @required Result yearSelected(_YearSelected value),
    @required Result carVariantSelected(_CarVariantSelected value),
    @required Result categorySelected(_CategorySelected value),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return manufacturerSelected(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectionStarted(_SelectionStarted value),
    Result manufacturerSelected(_ManufacturerSelected value),
    Result carModelSelected(_CarModelSelected value),
    Result yearSelected(_YearSelected value),
    Result carVariantSelected(_CarVariantSelected value),
    Result categorySelected(_CategorySelected value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (manufacturerSelected != null) {
      return manufacturerSelected(this);
    }
    return orElse();
  }
}

abstract class _ManufacturerSelected implements SelectionEvent {
  const factory _ManufacturerSelected(
      String manufacturerID, String manufacturer) = _$_ManufacturerSelected;

  String get manufacturerID;
  String get manufacturer;
  _$ManufacturerSelectedCopyWith<_ManufacturerSelected> get copyWith;
}

abstract class _$CarModelSelectedCopyWith<$Res> {
  factory _$CarModelSelectedCopyWith(
          _CarModelSelected value, $Res Function(_CarModelSelected) then) =
      __$CarModelSelectedCopyWithImpl<$Res>;
  $Res call({String carModelID, String carModel});
}

class __$CarModelSelectedCopyWithImpl<$Res>
    extends _$SelectionEventCopyWithImpl<$Res>
    implements _$CarModelSelectedCopyWith<$Res> {
  __$CarModelSelectedCopyWithImpl(
      _CarModelSelected _value, $Res Function(_CarModelSelected) _then)
      : super(_value, (v) => _then(v as _CarModelSelected));

  @override
  _CarModelSelected get _value => super._value as _CarModelSelected;

  @override
  $Res call({
    Object carModelID = freezed,
    Object carModel = freezed,
  }) {
    return _then(_CarModelSelected(
      carModelID == freezed ? _value.carModelID : carModelID as String,
      carModel == freezed ? _value.carModel : carModel as String,
    ));
  }
}

class _$_CarModelSelected implements _CarModelSelected {
  const _$_CarModelSelected(this.carModelID, this.carModel)
      : assert(carModelID != null),
        assert(carModel != null);

  @override
  final String carModelID;
  @override
  final String carModel;

  @override
  String toString() {
    return 'SelectionEvent.carModelSelected(carModelID: $carModelID, carModel: $carModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CarModelSelected &&
            (identical(other.carModelID, carModelID) ||
                const DeepCollectionEquality()
                    .equals(other.carModelID, carModelID)) &&
            (identical(other.carModel, carModel) ||
                const DeepCollectionEquality()
                    .equals(other.carModel, carModel)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(carModelID) ^
      const DeepCollectionEquality().hash(carModel);

  @override
  _$CarModelSelectedCopyWith<_CarModelSelected> get copyWith =>
      __$CarModelSelectedCopyWithImpl<_CarModelSelected>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectionStarted(),
    @required
        Result manufacturerSelected(String manufacturerID, String manufacturer),
    @required Result carModelSelected(String carModelID, String carModel),
    @required
        Result yearSelected(String year, String carModelID, String carModel),
    @required Result carVariantSelected(String carVariantID, String carVariant),
    @required Result categorySelected(String categoryID, String category),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return carModelSelected(carModelID, carModel);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectionStarted(),
    Result manufacturerSelected(String manufacturerID, String manufacturer),
    Result carModelSelected(String carModelID, String carModel),
    Result yearSelected(String year, String carModelID, String carModel),
    Result carVariantSelected(String carVariantID, String carVariant),
    Result categorySelected(String categoryID, String category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (carModelSelected != null) {
      return carModelSelected(carModelID, carModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectionStarted(_SelectionStarted value),
    @required Result manufacturerSelected(_ManufacturerSelected value),
    @required Result carModelSelected(_CarModelSelected value),
    @required Result yearSelected(_YearSelected value),
    @required Result carVariantSelected(_CarVariantSelected value),
    @required Result categorySelected(_CategorySelected value),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return carModelSelected(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectionStarted(_SelectionStarted value),
    Result manufacturerSelected(_ManufacturerSelected value),
    Result carModelSelected(_CarModelSelected value),
    Result yearSelected(_YearSelected value),
    Result carVariantSelected(_CarVariantSelected value),
    Result categorySelected(_CategorySelected value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (carModelSelected != null) {
      return carModelSelected(this);
    }
    return orElse();
  }
}

abstract class _CarModelSelected implements SelectionEvent {
  const factory _CarModelSelected(String carModelID, String carModel) =
      _$_CarModelSelected;

  String get carModelID;
  String get carModel;
  _$CarModelSelectedCopyWith<_CarModelSelected> get copyWith;
}

abstract class _$YearSelectedCopyWith<$Res> {
  factory _$YearSelectedCopyWith(
          _YearSelected value, $Res Function(_YearSelected) then) =
      __$YearSelectedCopyWithImpl<$Res>;
  $Res call({String year, String carModelID, String carModel});
}

class __$YearSelectedCopyWithImpl<$Res>
    extends _$SelectionEventCopyWithImpl<$Res>
    implements _$YearSelectedCopyWith<$Res> {
  __$YearSelectedCopyWithImpl(
      _YearSelected _value, $Res Function(_YearSelected) _then)
      : super(_value, (v) => _then(v as _YearSelected));

  @override
  _YearSelected get _value => super._value as _YearSelected;

  @override
  $Res call({
    Object year = freezed,
    Object carModelID = freezed,
    Object carModel = freezed,
  }) {
    return _then(_YearSelected(
      year == freezed ? _value.year : year as String,
      carModelID == freezed ? _value.carModelID : carModelID as String,
      carModel == freezed ? _value.carModel : carModel as String,
    ));
  }
}

class _$_YearSelected implements _YearSelected {
  const _$_YearSelected(this.year, this.carModelID, this.carModel)
      : assert(year != null),
        assert(carModelID != null),
        assert(carModel != null);

  @override
  final String year;
  @override
  final String carModelID;
  @override
  final String carModel;

  @override
  String toString() {
    return 'SelectionEvent.yearSelected(year: $year, carModelID: $carModelID, carModel: $carModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _YearSelected &&
            (identical(other.year, year) ||
                const DeepCollectionEquality().equals(other.year, year)) &&
            (identical(other.carModelID, carModelID) ||
                const DeepCollectionEquality()
                    .equals(other.carModelID, carModelID)) &&
            (identical(other.carModel, carModel) ||
                const DeepCollectionEquality()
                    .equals(other.carModel, carModel)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(year) ^
      const DeepCollectionEquality().hash(carModelID) ^
      const DeepCollectionEquality().hash(carModel);

  @override
  _$YearSelectedCopyWith<_YearSelected> get copyWith =>
      __$YearSelectedCopyWithImpl<_YearSelected>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectionStarted(),
    @required
        Result manufacturerSelected(String manufacturerID, String manufacturer),
    @required Result carModelSelected(String carModelID, String carModel),
    @required
        Result yearSelected(String year, String carModelID, String carModel),
    @required Result carVariantSelected(String carVariantID, String carVariant),
    @required Result categorySelected(String categoryID, String category),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return yearSelected(year, carModelID, carModel);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectionStarted(),
    Result manufacturerSelected(String manufacturerID, String manufacturer),
    Result carModelSelected(String carModelID, String carModel),
    Result yearSelected(String year, String carModelID, String carModel),
    Result carVariantSelected(String carVariantID, String carVariant),
    Result categorySelected(String categoryID, String category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (yearSelected != null) {
      return yearSelected(year, carModelID, carModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectionStarted(_SelectionStarted value),
    @required Result manufacturerSelected(_ManufacturerSelected value),
    @required Result carModelSelected(_CarModelSelected value),
    @required Result yearSelected(_YearSelected value),
    @required Result carVariantSelected(_CarVariantSelected value),
    @required Result categorySelected(_CategorySelected value),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return yearSelected(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectionStarted(_SelectionStarted value),
    Result manufacturerSelected(_ManufacturerSelected value),
    Result carModelSelected(_CarModelSelected value),
    Result yearSelected(_YearSelected value),
    Result carVariantSelected(_CarVariantSelected value),
    Result categorySelected(_CategorySelected value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (yearSelected != null) {
      return yearSelected(this);
    }
    return orElse();
  }
}

abstract class _YearSelected implements SelectionEvent {
  const factory _YearSelected(String year, String carModelID, String carModel) =
      _$_YearSelected;

  String get year;
  String get carModelID;
  String get carModel;
  _$YearSelectedCopyWith<_YearSelected> get copyWith;
}

abstract class _$CarVariantSelectedCopyWith<$Res> {
  factory _$CarVariantSelectedCopyWith(
          _CarVariantSelected value, $Res Function(_CarVariantSelected) then) =
      __$CarVariantSelectedCopyWithImpl<$Res>;
  $Res call({String carVariantID, String carVariant});
}

class __$CarVariantSelectedCopyWithImpl<$Res>
    extends _$SelectionEventCopyWithImpl<$Res>
    implements _$CarVariantSelectedCopyWith<$Res> {
  __$CarVariantSelectedCopyWithImpl(
      _CarVariantSelected _value, $Res Function(_CarVariantSelected) _then)
      : super(_value, (v) => _then(v as _CarVariantSelected));

  @override
  _CarVariantSelected get _value => super._value as _CarVariantSelected;

  @override
  $Res call({
    Object carVariantID = freezed,
    Object carVariant = freezed,
  }) {
    return _then(_CarVariantSelected(
      carVariantID == freezed ? _value.carVariantID : carVariantID as String,
      carVariant == freezed ? _value.carVariant : carVariant as String,
    ));
  }
}

class _$_CarVariantSelected implements _CarVariantSelected {
  const _$_CarVariantSelected(this.carVariantID, this.carVariant)
      : assert(carVariantID != null),
        assert(carVariant != null);

  @override
  final String carVariantID;
  @override
  final String carVariant;

  @override
  String toString() {
    return 'SelectionEvent.carVariantSelected(carVariantID: $carVariantID, carVariant: $carVariant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CarVariantSelected &&
            (identical(other.carVariantID, carVariantID) ||
                const DeepCollectionEquality()
                    .equals(other.carVariantID, carVariantID)) &&
            (identical(other.carVariant, carVariant) ||
                const DeepCollectionEquality()
                    .equals(other.carVariant, carVariant)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(carVariantID) ^
      const DeepCollectionEquality().hash(carVariant);

  @override
  _$CarVariantSelectedCopyWith<_CarVariantSelected> get copyWith =>
      __$CarVariantSelectedCopyWithImpl<_CarVariantSelected>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectionStarted(),
    @required
        Result manufacturerSelected(String manufacturerID, String manufacturer),
    @required Result carModelSelected(String carModelID, String carModel),
    @required
        Result yearSelected(String year, String carModelID, String carModel),
    @required Result carVariantSelected(String carVariantID, String carVariant),
    @required Result categorySelected(String categoryID, String category),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return carVariantSelected(carVariantID, carVariant);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectionStarted(),
    Result manufacturerSelected(String manufacturerID, String manufacturer),
    Result carModelSelected(String carModelID, String carModel),
    Result yearSelected(String year, String carModelID, String carModel),
    Result carVariantSelected(String carVariantID, String carVariant),
    Result categorySelected(String categoryID, String category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (carVariantSelected != null) {
      return carVariantSelected(carVariantID, carVariant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectionStarted(_SelectionStarted value),
    @required Result manufacturerSelected(_ManufacturerSelected value),
    @required Result carModelSelected(_CarModelSelected value),
    @required Result yearSelected(_YearSelected value),
    @required Result carVariantSelected(_CarVariantSelected value),
    @required Result categorySelected(_CategorySelected value),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return carVariantSelected(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectionStarted(_SelectionStarted value),
    Result manufacturerSelected(_ManufacturerSelected value),
    Result carModelSelected(_CarModelSelected value),
    Result yearSelected(_YearSelected value),
    Result carVariantSelected(_CarVariantSelected value),
    Result categorySelected(_CategorySelected value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (carVariantSelected != null) {
      return carVariantSelected(this);
    }
    return orElse();
  }
}

abstract class _CarVariantSelected implements SelectionEvent {
  const factory _CarVariantSelected(String carVariantID, String carVariant) =
      _$_CarVariantSelected;

  String get carVariantID;
  String get carVariant;
  _$CarVariantSelectedCopyWith<_CarVariantSelected> get copyWith;
}

abstract class _$CategorySelectedCopyWith<$Res> {
  factory _$CategorySelectedCopyWith(
          _CategorySelected value, $Res Function(_CategorySelected) then) =
      __$CategorySelectedCopyWithImpl<$Res>;
  $Res call({String categoryID, String category});
}

class __$CategorySelectedCopyWithImpl<$Res>
    extends _$SelectionEventCopyWithImpl<$Res>
    implements _$CategorySelectedCopyWith<$Res> {
  __$CategorySelectedCopyWithImpl(
      _CategorySelected _value, $Res Function(_CategorySelected) _then)
      : super(_value, (v) => _then(v as _CategorySelected));

  @override
  _CategorySelected get _value => super._value as _CategorySelected;

  @override
  $Res call({
    Object categoryID = freezed,
    Object category = freezed,
  }) {
    return _then(_CategorySelected(
      categoryID == freezed ? _value.categoryID : categoryID as String,
      category == freezed ? _value.category : category as String,
    ));
  }
}

class _$_CategorySelected implements _CategorySelected {
  const _$_CategorySelected(this.categoryID, this.category)
      : assert(categoryID != null),
        assert(category != null);

  @override
  final String categoryID;
  @override
  final String category;

  @override
  String toString() {
    return 'SelectionEvent.categorySelected(categoryID: $categoryID, category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CategorySelected &&
            (identical(other.categoryID, categoryID) ||
                const DeepCollectionEquality()
                    .equals(other.categoryID, categoryID)) &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(categoryID) ^
      const DeepCollectionEquality().hash(category);

  @override
  _$CategorySelectedCopyWith<_CategorySelected> get copyWith =>
      __$CategorySelectedCopyWithImpl<_CategorySelected>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result selectionStarted(),
    @required
        Result manufacturerSelected(String manufacturerID, String manufacturer),
    @required Result carModelSelected(String carModelID, String carModel),
    @required
        Result yearSelected(String year, String carModelID, String carModel),
    @required Result carVariantSelected(String carVariantID, String carVariant),
    @required Result categorySelected(String categoryID, String category),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return categorySelected(categoryID, category);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result selectionStarted(),
    Result manufacturerSelected(String manufacturerID, String manufacturer),
    Result carModelSelected(String carModelID, String carModel),
    Result yearSelected(String year, String carModelID, String carModel),
    Result carVariantSelected(String carVariantID, String carVariant),
    Result categorySelected(String categoryID, String category),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (categorySelected != null) {
      return categorySelected(categoryID, category);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result selectionStarted(_SelectionStarted value),
    @required Result manufacturerSelected(_ManufacturerSelected value),
    @required Result carModelSelected(_CarModelSelected value),
    @required Result yearSelected(_YearSelected value),
    @required Result carVariantSelected(_CarVariantSelected value),
    @required Result categorySelected(_CategorySelected value),
  }) {
    assert(selectionStarted != null);
    assert(manufacturerSelected != null);
    assert(carModelSelected != null);
    assert(yearSelected != null);
    assert(carVariantSelected != null);
    assert(categorySelected != null);
    return categorySelected(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result selectionStarted(_SelectionStarted value),
    Result manufacturerSelected(_ManufacturerSelected value),
    Result carModelSelected(_CarModelSelected value),
    Result yearSelected(_YearSelected value),
    Result carVariantSelected(_CarVariantSelected value),
    Result categorySelected(_CategorySelected value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (categorySelected != null) {
      return categorySelected(this);
    }
    return orElse();
  }
}

abstract class _CategorySelected implements SelectionEvent {
  const factory _CategorySelected(String categoryID, String category) =
      _$_CategorySelected;

  String get categoryID;
  String get category;
  _$CategorySelectedCopyWith<_CategorySelected> get copyWith;
}

class _$SelectionStateTearOff {
  const _$SelectionStateTearOff();

// ignore: unused_element
  _SelectionState call(
      {@required Selection selection,
      @required List<Map<String, dynamic>> manufacturers,
      @required List<Map<String, dynamic>> carModels,
      @required List<CarVariant> carVariants,
      @required List<Map<String, dynamic>> categories,
      @required bool isLoading,
      @required bool isError}) {
    return _SelectionState(
      selection: selection,
      manufacturers: manufacturers,
      carModels: carModels,
      carVariants: carVariants,
      categories: categories,
      isLoading: isLoading,
      isError: isError,
    );
  }
}

// ignore: unused_element
const $SelectionState = _$SelectionStateTearOff();

mixin _$SelectionState {
  Selection get selection;
  List<Map<String, dynamic>> get manufacturers;
  List<Map<String, dynamic>> get carModels;
  List<CarVariant> get carVariants;
  List<Map<String, dynamic>> get categories;
  bool get isLoading;
  bool get isError;

  $SelectionStateCopyWith<SelectionState> get copyWith;
}

abstract class $SelectionStateCopyWith<$Res> {
  factory $SelectionStateCopyWith(
          SelectionState value, $Res Function(SelectionState) then) =
      _$SelectionStateCopyWithImpl<$Res>;
  $Res call(
      {Selection selection,
      List<Map<String, dynamic>> manufacturers,
      List<Map<String, dynamic>> carModels,
      List<CarVariant> carVariants,
      List<Map<String, dynamic>> categories,
      bool isLoading,
      bool isError});

  $SelectionCopyWith<$Res> get selection;
}

class _$SelectionStateCopyWithImpl<$Res>
    implements $SelectionStateCopyWith<$Res> {
  _$SelectionStateCopyWithImpl(this._value, this._then);

  final SelectionState _value;
  // ignore: unused_field
  final $Res Function(SelectionState) _then;

  @override
  $Res call({
    Object selection = freezed,
    Object manufacturers = freezed,
    Object carModels = freezed,
    Object carVariants = freezed,
    Object categories = freezed,
    Object isLoading = freezed,
    Object isError = freezed,
  }) {
    return _then(_value.copyWith(
      selection:
          selection == freezed ? _value.selection : selection as Selection,
      manufacturers: manufacturers == freezed
          ? _value.manufacturers
          : manufacturers as List<Map<String, dynamic>>,
      carModels: carModels == freezed
          ? _value.carModels
          : carModels as List<Map<String, dynamic>>,
      carVariants: carVariants == freezed
          ? _value.carVariants
          : carVariants as List<CarVariant>,
      categories: categories == freezed
          ? _value.categories
          : categories as List<Map<String, dynamic>>,
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isError: isError == freezed ? _value.isError : isError as bool,
    ));
  }

  @override
  $SelectionCopyWith<$Res> get selection {
    if (_value.selection == null) {
      return null;
    }
    return $SelectionCopyWith<$Res>(_value.selection, (value) {
      return _then(_value.copyWith(selection: value));
    });
  }
}

abstract class _$SelectionStateCopyWith<$Res>
    implements $SelectionStateCopyWith<$Res> {
  factory _$SelectionStateCopyWith(
          _SelectionState value, $Res Function(_SelectionState) then) =
      __$SelectionStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Selection selection,
      List<Map<String, dynamic>> manufacturers,
      List<Map<String, dynamic>> carModels,
      List<CarVariant> carVariants,
      List<Map<String, dynamic>> categories,
      bool isLoading,
      bool isError});

  @override
  $SelectionCopyWith<$Res> get selection;
}

class __$SelectionStateCopyWithImpl<$Res>
    extends _$SelectionStateCopyWithImpl<$Res>
    implements _$SelectionStateCopyWith<$Res> {
  __$SelectionStateCopyWithImpl(
      _SelectionState _value, $Res Function(_SelectionState) _then)
      : super(_value, (v) => _then(v as _SelectionState));

  @override
  _SelectionState get _value => super._value as _SelectionState;

  @override
  $Res call({
    Object selection = freezed,
    Object manufacturers = freezed,
    Object carModels = freezed,
    Object carVariants = freezed,
    Object categories = freezed,
    Object isLoading = freezed,
    Object isError = freezed,
  }) {
    return _then(_SelectionState(
      selection:
          selection == freezed ? _value.selection : selection as Selection,
      manufacturers: manufacturers == freezed
          ? _value.manufacturers
          : manufacturers as List<Map<String, dynamic>>,
      carModels: carModels == freezed
          ? _value.carModels
          : carModels as List<Map<String, dynamic>>,
      carVariants: carVariants == freezed
          ? _value.carVariants
          : carVariants as List<CarVariant>,
      categories: categories == freezed
          ? _value.categories
          : categories as List<Map<String, dynamic>>,
      isLoading: isLoading == freezed ? _value.isLoading : isLoading as bool,
      isError: isError == freezed ? _value.isError : isError as bool,
    ));
  }
}

class _$_SelectionState implements _SelectionState {
  const _$_SelectionState(
      {@required this.selection,
      @required this.manufacturers,
      @required this.carModels,
      @required this.carVariants,
      @required this.categories,
      @required this.isLoading,
      @required this.isError})
      : assert(selection != null),
        assert(manufacturers != null),
        assert(carModels != null),
        assert(carVariants != null),
        assert(categories != null),
        assert(isLoading != null),
        assert(isError != null);

  @override
  final Selection selection;
  @override
  final List<Map<String, dynamic>> manufacturers;
  @override
  final List<Map<String, dynamic>> carModels;
  @override
  final List<CarVariant> carVariants;
  @override
  final List<Map<String, dynamic>> categories;
  @override
  final bool isLoading;
  @override
  final bool isError;

  @override
  String toString() {
    return 'SelectionState(selection: $selection, manufacturers: $manufacturers, carModels: $carModels, carVariants: $carVariants, categories: $categories, isLoading: $isLoading, isError: $isError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SelectionState &&
            (identical(other.selection, selection) ||
                const DeepCollectionEquality()
                    .equals(other.selection, selection)) &&
            (identical(other.manufacturers, manufacturers) ||
                const DeepCollectionEquality()
                    .equals(other.manufacturers, manufacturers)) &&
            (identical(other.carModels, carModels) ||
                const DeepCollectionEquality()
                    .equals(other.carModels, carModels)) &&
            (identical(other.carVariants, carVariants) ||
                const DeepCollectionEquality()
                    .equals(other.carVariants, carVariants)) &&
            (identical(other.categories, categories) ||
                const DeepCollectionEquality()
                    .equals(other.categories, categories)) &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)) &&
            (identical(other.isError, isError) ||
                const DeepCollectionEquality().equals(other.isError, isError)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(selection) ^
      const DeepCollectionEquality().hash(manufacturers) ^
      const DeepCollectionEquality().hash(carModels) ^
      const DeepCollectionEquality().hash(carVariants) ^
      const DeepCollectionEquality().hash(categories) ^
      const DeepCollectionEquality().hash(isLoading) ^
      const DeepCollectionEquality().hash(isError);

  @override
  _$SelectionStateCopyWith<_SelectionState> get copyWith =>
      __$SelectionStateCopyWithImpl<_SelectionState>(this, _$identity);
}

abstract class _SelectionState implements SelectionState {
  const factory _SelectionState(
      {@required Selection selection,
      @required List<Map<String, dynamic>> manufacturers,
      @required List<Map<String, dynamic>> carModels,
      @required List<CarVariant> carVariants,
      @required List<Map<String, dynamic>> categories,
      @required bool isLoading,
      @required bool isError}) = _$_SelectionState;

  @override
  Selection get selection;
  @override
  List<Map<String, dynamic>> get manufacturers;
  @override
  List<Map<String, dynamic>> get carModels;
  @override
  List<CarVariant> get carVariants;
  @override
  List<Map<String, dynamic>> get categories;
  @override
  bool get isLoading;
  @override
  bool get isError;
  @override
  _$SelectionStateCopyWith<_SelectionState> get copyWith;
}
