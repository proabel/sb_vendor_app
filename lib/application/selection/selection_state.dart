part of 'selection_bloc.dart';


@freezed
abstract class SelectionState with _$SelectionState {
  const factory SelectionState({
    @required Selection selection,
    @required List<Map<String, dynamic>> manufacturers,
    @required List<Map<String, dynamic>> carModels,
    @required List<CarVariant> carVariants,
    @required List<Map<String, dynamic>> categories,
    @required bool isLoading,
    @required bool isError,
  }) = _SelectionState;

  factory SelectionState.initial() => SelectionState(
    selection: Selection.empty(),
    manufacturers: [],
    carModels: [],
    carVariants: [],
    categories: [],
    isLoading: false,
    isError: false
  );

  //const factory SelectionState.loading() = _Loading;

}