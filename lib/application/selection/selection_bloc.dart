import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:sb_vendor/domain/selection/i_selection_repository.dart';
import 'package:sb_vendor/domain/selection/selection.dart';
import 'package:sb_vendor/domain/vendor_product/car_variant.dart';

part 'selection_event.dart';
part 'selection_state.dart';
part 'selection_bloc.freezed.dart';

@injectable
class SelectionBloc extends Bloc<SelectionEvent, SelectionState> {
  final ISelectionRepository _selectionRepository;

  SelectionBloc(this._selectionRepository) : super(SelectionState.initial());

  StreamSubscription _SelectionStreamSubscription;

  @override
  Stream<SelectionState> mapEventToState(
    SelectionEvent event,
  ) async* {
    yield* event.map(
        selectionStarted: (e) async* {
          yield SelectionState.initial();
          final manufacturers = await _selectionRepository.getManufacturers();
          yield SelectionState.initial().copyWith(manufacturers: manufacturers);
        },
        manufacturerSelected: (e) async* {
          yield state.copyWith(isLoading: true);
          final carModels = await _selectionRepository.getCarModels(e.manufacturerID);
          final selection  = state.selection.copyWith(manufacturerID: e.manufacturerID, manufacturer: e.manufacturer);
          yield state.copyWith(carModels: carModels, isLoading: false, selection: selection);
        },
        carModelSelected: (e) async* {
          final selection  = state.selection.copyWith(carModelID: e.carModelID, carModel: e.carModel);
          yield state.copyWith(selection: selection);
        },
        yearSelected: (e) async* {
          yield state.copyWith(isLoading: true);
          final carVariants = await _selectionRepository.getVariants(e.year, e.carModelID);
          final selection  = state.selection.copyWith(year: e.year);
          yield state.copyWith(selection: selection, carVariants: carVariants, isLoading: false);
        },
        carVariantSelected: (e) async* {
          yield state.copyWith(isLoading: true);
          final categories = await _selectionRepository.getCategories();
          final selection  = state.selection.copyWith(carVariantID: e.carVariantID, carVariant: e.carVariant);
          yield state.copyWith(selection: selection, categories: categories, isLoading: false);
        },
        categorySelected: (e) async* {
          final selection  = state.selection.copyWith(categoryID: e.categoryID, category: e.category);
          yield state.copyWith(selection: selection);
        }
    );
  }
  @override
  Future<void> close() async {
    await _SelectionStreamSubscription?.cancel();
    return super.close();
  }
}
