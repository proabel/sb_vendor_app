part of 'sign_in_form_bloc.dart';


@freezed
abstract class SignInFormState with _$SignInFormState {
  const factory SignInFormState({
    @required String emailAddress,
    @required String password,
    @required bool isSubmitting,
    @required bool showErrorMessage,
    @required bool showRegisterPage,
  }) = _SignInFormState;

  factory SignInFormState.initial() => SignInFormState(
    emailAddress: '',
    password: '',
    isSubmitting: false,
    showErrorMessage: false,
    showRegisterPage: false,
  );

}