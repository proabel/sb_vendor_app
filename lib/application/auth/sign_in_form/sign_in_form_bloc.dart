import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:sb_vendor/domain/auth/i_auth_repository.dart';

part 'sign_in_form_event.dart';
part 'sign_in_form_state.dart';
part 'sign_in_form_bloc.freezed.dart';

@injectable
class SignInFormBloc extends Bloc<SignInFormEvent, SignInFormState> {
  final IAuthRepository _authRepo;

  SignInFormBloc(this._authRepo) : super(null);

  @override
  SignInFormState get initialState => SignInFormState.initial();

  @override
  Stream<SignInFormState> mapEventToState(
    SignInFormEvent event,
  ) async* {
    yield* event.map(
      signInPressed: (e) async* {
        yield state.copyWith(
          isSubmitting : true
        );
        _authRepo.signInWithEmailAndPassword(emailAddress: e.emailAddress, password: e.password);
      }, 
      showRegisterPresed: (e) async* {
        yield state.copyWith(
          showRegisterPage: true
        );
      }
    );
  }
}
