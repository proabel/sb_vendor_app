// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'sign_in_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SignInFormEventTearOff {
  const _$SignInFormEventTearOff();

// ignore: unused_element
  SignInPressed signInPressed(String emailAddress, String password) {
    return SignInPressed(
      emailAddress,
      password,
    );
  }

// ignore: unused_element
  ShowRegisterPage showRegisterPresed() {
    return const ShowRegisterPage();
  }
}

// ignore: unused_element
const $SignInFormEvent = _$SignInFormEventTearOff();

mixin _$SignInFormEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result signInPressed(String emailAddress, String password),
    @required Result showRegisterPresed(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result signInPressed(String emailAddress, String password),
    Result showRegisterPresed(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result signInPressed(SignInPressed value),
    @required Result showRegisterPresed(ShowRegisterPage value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result signInPressed(SignInPressed value),
    Result showRegisterPresed(ShowRegisterPage value),
    @required Result orElse(),
  });
}

abstract class $SignInFormEventCopyWith<$Res> {
  factory $SignInFormEventCopyWith(
          SignInFormEvent value, $Res Function(SignInFormEvent) then) =
      _$SignInFormEventCopyWithImpl<$Res>;
}

class _$SignInFormEventCopyWithImpl<$Res>
    implements $SignInFormEventCopyWith<$Res> {
  _$SignInFormEventCopyWithImpl(this._value, this._then);

  final SignInFormEvent _value;
  // ignore: unused_field
  final $Res Function(SignInFormEvent) _then;
}

abstract class $SignInPressedCopyWith<$Res> {
  factory $SignInPressedCopyWith(
          SignInPressed value, $Res Function(SignInPressed) then) =
      _$SignInPressedCopyWithImpl<$Res>;
  $Res call({String emailAddress, String password});
}

class _$SignInPressedCopyWithImpl<$Res>
    extends _$SignInFormEventCopyWithImpl<$Res>
    implements $SignInPressedCopyWith<$Res> {
  _$SignInPressedCopyWithImpl(
      SignInPressed _value, $Res Function(SignInPressed) _then)
      : super(_value, (v) => _then(v as SignInPressed));

  @override
  SignInPressed get _value => super._value as SignInPressed;

  @override
  $Res call({
    Object emailAddress = freezed,
    Object password = freezed,
  }) {
    return _then(SignInPressed(
      emailAddress == freezed ? _value.emailAddress : emailAddress as String,
      password == freezed ? _value.password : password as String,
    ));
  }
}

class _$SignInPressed implements SignInPressed {
  const _$SignInPressed(this.emailAddress, this.password)
      : assert(emailAddress != null),
        assert(password != null);

  @override
  final String emailAddress;
  @override
  final String password;

  @override
  String toString() {
    return 'SignInFormEvent.signInPressed(emailAddress: $emailAddress, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignInPressed &&
            (identical(other.emailAddress, emailAddress) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddress, emailAddress)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(emailAddress) ^
      const DeepCollectionEquality().hash(password);

  @override
  $SignInPressedCopyWith<SignInPressed> get copyWith =>
      _$SignInPressedCopyWithImpl<SignInPressed>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result signInPressed(String emailAddress, String password),
    @required Result showRegisterPresed(),
  }) {
    assert(signInPressed != null);
    assert(showRegisterPresed != null);
    return signInPressed(emailAddress, password);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result signInPressed(String emailAddress, String password),
    Result showRegisterPresed(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signInPressed != null) {
      return signInPressed(emailAddress, password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result signInPressed(SignInPressed value),
    @required Result showRegisterPresed(ShowRegisterPage value),
  }) {
    assert(signInPressed != null);
    assert(showRegisterPresed != null);
    return signInPressed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result signInPressed(SignInPressed value),
    Result showRegisterPresed(ShowRegisterPage value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signInPressed != null) {
      return signInPressed(this);
    }
    return orElse();
  }
}

abstract class SignInPressed implements SignInFormEvent {
  const factory SignInPressed(String emailAddress, String password) =
      _$SignInPressed;

  String get emailAddress;
  String get password;
  $SignInPressedCopyWith<SignInPressed> get copyWith;
}

abstract class $ShowRegisterPageCopyWith<$Res> {
  factory $ShowRegisterPageCopyWith(
          ShowRegisterPage value, $Res Function(ShowRegisterPage) then) =
      _$ShowRegisterPageCopyWithImpl<$Res>;
}

class _$ShowRegisterPageCopyWithImpl<$Res>
    extends _$SignInFormEventCopyWithImpl<$Res>
    implements $ShowRegisterPageCopyWith<$Res> {
  _$ShowRegisterPageCopyWithImpl(
      ShowRegisterPage _value, $Res Function(ShowRegisterPage) _then)
      : super(_value, (v) => _then(v as ShowRegisterPage));

  @override
  ShowRegisterPage get _value => super._value as ShowRegisterPage;
}

class _$ShowRegisterPage implements ShowRegisterPage {
  const _$ShowRegisterPage();

  @override
  String toString() {
    return 'SignInFormEvent.showRegisterPresed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ShowRegisterPage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result signInPressed(String emailAddress, String password),
    @required Result showRegisterPresed(),
  }) {
    assert(signInPressed != null);
    assert(showRegisterPresed != null);
    return showRegisterPresed();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result signInPressed(String emailAddress, String password),
    Result showRegisterPresed(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (showRegisterPresed != null) {
      return showRegisterPresed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result signInPressed(SignInPressed value),
    @required Result showRegisterPresed(ShowRegisterPage value),
  }) {
    assert(signInPressed != null);
    assert(showRegisterPresed != null);
    return showRegisterPresed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result signInPressed(SignInPressed value),
    Result showRegisterPresed(ShowRegisterPage value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (showRegisterPresed != null) {
      return showRegisterPresed(this);
    }
    return orElse();
  }
}

abstract class ShowRegisterPage implements SignInFormEvent {
  const factory ShowRegisterPage() = _$ShowRegisterPage;
}

class _$SignInFormStateTearOff {
  const _$SignInFormStateTearOff();

// ignore: unused_element
  _SignInFormState call(
      {@required String emailAddress,
      @required String password,
      @required bool isSubmitting,
      @required bool showErrorMessage,
      @required bool showRegisterPage}) {
    return _SignInFormState(
      emailAddress: emailAddress,
      password: password,
      isSubmitting: isSubmitting,
      showErrorMessage: showErrorMessage,
      showRegisterPage: showRegisterPage,
    );
  }
}

// ignore: unused_element
const $SignInFormState = _$SignInFormStateTearOff();

mixin _$SignInFormState {
  String get emailAddress;
  String get password;
  bool get isSubmitting;
  bool get showErrorMessage;
  bool get showRegisterPage;

  $SignInFormStateCopyWith<SignInFormState> get copyWith;
}

abstract class $SignInFormStateCopyWith<$Res> {
  factory $SignInFormStateCopyWith(
          SignInFormState value, $Res Function(SignInFormState) then) =
      _$SignInFormStateCopyWithImpl<$Res>;
  $Res call(
      {String emailAddress,
      String password,
      bool isSubmitting,
      bool showErrorMessage,
      bool showRegisterPage});
}

class _$SignInFormStateCopyWithImpl<$Res>
    implements $SignInFormStateCopyWith<$Res> {
  _$SignInFormStateCopyWithImpl(this._value, this._then);

  final SignInFormState _value;
  // ignore: unused_field
  final $Res Function(SignInFormState) _then;

  @override
  $Res call({
    Object emailAddress = freezed,
    Object password = freezed,
    Object isSubmitting = freezed,
    Object showErrorMessage = freezed,
    Object showRegisterPage = freezed,
  }) {
    return _then(_value.copyWith(
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as String,
      password: password == freezed ? _value.password : password as String,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      showErrorMessage: showErrorMessage == freezed
          ? _value.showErrorMessage
          : showErrorMessage as bool,
      showRegisterPage: showRegisterPage == freezed
          ? _value.showRegisterPage
          : showRegisterPage as bool,
    ));
  }
}

abstract class _$SignInFormStateCopyWith<$Res>
    implements $SignInFormStateCopyWith<$Res> {
  factory _$SignInFormStateCopyWith(
          _SignInFormState value, $Res Function(_SignInFormState) then) =
      __$SignInFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {String emailAddress,
      String password,
      bool isSubmitting,
      bool showErrorMessage,
      bool showRegisterPage});
}

class __$SignInFormStateCopyWithImpl<$Res>
    extends _$SignInFormStateCopyWithImpl<$Res>
    implements _$SignInFormStateCopyWith<$Res> {
  __$SignInFormStateCopyWithImpl(
      _SignInFormState _value, $Res Function(_SignInFormState) _then)
      : super(_value, (v) => _then(v as _SignInFormState));

  @override
  _SignInFormState get _value => super._value as _SignInFormState;

  @override
  $Res call({
    Object emailAddress = freezed,
    Object password = freezed,
    Object isSubmitting = freezed,
    Object showErrorMessage = freezed,
    Object showRegisterPage = freezed,
  }) {
    return _then(_SignInFormState(
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as String,
      password: password == freezed ? _value.password : password as String,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      showErrorMessage: showErrorMessage == freezed
          ? _value.showErrorMessage
          : showErrorMessage as bool,
      showRegisterPage: showRegisterPage == freezed
          ? _value.showRegisterPage
          : showRegisterPage as bool,
    ));
  }
}

class _$_SignInFormState implements _SignInFormState {
  const _$_SignInFormState(
      {@required this.emailAddress,
      @required this.password,
      @required this.isSubmitting,
      @required this.showErrorMessage,
      @required this.showRegisterPage})
      : assert(emailAddress != null),
        assert(password != null),
        assert(isSubmitting != null),
        assert(showErrorMessage != null),
        assert(showRegisterPage != null);

  @override
  final String emailAddress;
  @override
  final String password;
  @override
  final bool isSubmitting;
  @override
  final bool showErrorMessage;
  @override
  final bool showRegisterPage;

  @override
  String toString() {
    return 'SignInFormState(emailAddress: $emailAddress, password: $password, isSubmitting: $isSubmitting, showErrorMessage: $showErrorMessage, showRegisterPage: $showRegisterPage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SignInFormState &&
            (identical(other.emailAddress, emailAddress) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddress, emailAddress)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.isSubmitting, isSubmitting) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitting, isSubmitting)) &&
            (identical(other.showErrorMessage, showErrorMessage) ||
                const DeepCollectionEquality()
                    .equals(other.showErrorMessage, showErrorMessage)) &&
            (identical(other.showRegisterPage, showRegisterPage) ||
                const DeepCollectionEquality()
                    .equals(other.showRegisterPage, showRegisterPage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(emailAddress) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(isSubmitting) ^
      const DeepCollectionEquality().hash(showErrorMessage) ^
      const DeepCollectionEquality().hash(showRegisterPage);

  @override
  _$SignInFormStateCopyWith<_SignInFormState> get copyWith =>
      __$SignInFormStateCopyWithImpl<_SignInFormState>(this, _$identity);
}

abstract class _SignInFormState implements SignInFormState {
  const factory _SignInFormState(
      {@required String emailAddress,
      @required String password,
      @required bool isSubmitting,
      @required bool showErrorMessage,
      @required bool showRegisterPage}) = _$_SignInFormState;

  @override
  String get emailAddress;
  @override
  String get password;
  @override
  bool get isSubmitting;
  @override
  bool get showErrorMessage;
  @override
  bool get showRegisterPage;
  @override
  _$SignInFormStateCopyWith<_SignInFormState> get copyWith;
}
