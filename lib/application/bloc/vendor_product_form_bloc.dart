import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:sb_vendor/domain/vendor_product/i_vendor_product_repository.dart';
import 'package:sb_vendor/domain/vendor_product/vendor_product.dart';

part 'vendor_product_form_event.dart';
part 'vendor_product_form_state.dart';
part 'vendor_product_form_bloc.freezed.dart';

class VendorProductFormBloc extends Bloc<VendorProductFormEvent, VendorProductFormState> {
  final IVendorProductRepository _vendorProductRepository;

  VendorProductFormBloc(this._vendorProductRepository) : super(VendorProductFormState.initial());

  @override
  Stream<VendorProductFormState> mapEventToState(
    VendorProductFormEvent event,
  ) async* {
    yield* event.map(createVendorProductClicked: (e) async* {
      yield _vendorProductRepository.createVendorProduct(e.vendorProduct);
    });
  }
}
