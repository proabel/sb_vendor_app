// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'vendor_product_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$VendorProductFormEventTearOff {
  const _$VendorProductFormEventTearOff();

// ignore: unused_element
  _CreateVendorProductClicked createVendorProductClicked(
      @required VendorProduct vendorProduct) {
    return _CreateVendorProductClicked(
      vendorProduct,
    );
  }
}

// ignore: unused_element
const $VendorProductFormEvent = _$VendorProductFormEventTearOff();

mixin _$VendorProductFormEvent {
  VendorProduct get vendorProduct;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result createVendorProductClicked(
            @required VendorProduct vendorProduct),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result createVendorProductClicked(@required VendorProduct vendorProduct),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result createVendorProductClicked(_CreateVendorProductClicked value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result createVendorProductClicked(_CreateVendorProductClicked value),
    @required Result orElse(),
  });

  $VendorProductFormEventCopyWith<VendorProductFormEvent> get copyWith;
}

abstract class $VendorProductFormEventCopyWith<$Res> {
  factory $VendorProductFormEventCopyWith(VendorProductFormEvent value,
          $Res Function(VendorProductFormEvent) then) =
      _$VendorProductFormEventCopyWithImpl<$Res>;
  $Res call({VendorProduct vendorProduct});

  $VendorProductCopyWith<$Res> get vendorProduct;
}

class _$VendorProductFormEventCopyWithImpl<$Res>
    implements $VendorProductFormEventCopyWith<$Res> {
  _$VendorProductFormEventCopyWithImpl(this._value, this._then);

  final VendorProductFormEvent _value;
  // ignore: unused_field
  final $Res Function(VendorProductFormEvent) _then;

  @override
  $Res call({
    Object vendorProduct = freezed,
  }) {
    return _then(_value.copyWith(
      vendorProduct: vendorProduct == freezed
          ? _value.vendorProduct
          : vendorProduct as VendorProduct,
    ));
  }

  @override
  $VendorProductCopyWith<$Res> get vendorProduct {
    if (_value.vendorProduct == null) {
      return null;
    }
    return $VendorProductCopyWith<$Res>(_value.vendorProduct, (value) {
      return _then(_value.copyWith(vendorProduct: value));
    });
  }
}

abstract class _$CreateVendorProductClickedCopyWith<$Res>
    implements $VendorProductFormEventCopyWith<$Res> {
  factory _$CreateVendorProductClickedCopyWith(
          _CreateVendorProductClicked value,
          $Res Function(_CreateVendorProductClicked) then) =
      __$CreateVendorProductClickedCopyWithImpl<$Res>;
  @override
  $Res call({VendorProduct vendorProduct});

  @override
  $VendorProductCopyWith<$Res> get vendorProduct;
}

class __$CreateVendorProductClickedCopyWithImpl<$Res>
    extends _$VendorProductFormEventCopyWithImpl<$Res>
    implements _$CreateVendorProductClickedCopyWith<$Res> {
  __$CreateVendorProductClickedCopyWithImpl(_CreateVendorProductClicked _value,
      $Res Function(_CreateVendorProductClicked) _then)
      : super(_value, (v) => _then(v as _CreateVendorProductClicked));

  @override
  _CreateVendorProductClicked get _value =>
      super._value as _CreateVendorProductClicked;

  @override
  $Res call({
    Object vendorProduct = freezed,
  }) {
    return _then(_CreateVendorProductClicked(
      vendorProduct == freezed
          ? _value.vendorProduct
          : vendorProduct as VendorProduct,
    ));
  }
}

class _$_CreateVendorProductClicked implements _CreateVendorProductClicked {
  const _$_CreateVendorProductClicked(@required this.vendorProduct)
      : assert(vendorProduct != null);

  @override
  final VendorProduct vendorProduct;

  @override
  String toString() {
    return 'VendorProductFormEvent.createVendorProductClicked(vendorProduct: $vendorProduct)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CreateVendorProductClicked &&
            (identical(other.vendorProduct, vendorProduct) ||
                const DeepCollectionEquality()
                    .equals(other.vendorProduct, vendorProduct)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(vendorProduct);

  @override
  _$CreateVendorProductClickedCopyWith<_CreateVendorProductClicked>
      get copyWith => __$CreateVendorProductClickedCopyWithImpl<
          _CreateVendorProductClicked>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result createVendorProductClicked(
            @required VendorProduct vendorProduct),
  }) {
    assert(createVendorProductClicked != null);
    return createVendorProductClicked(vendorProduct);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result createVendorProductClicked(@required VendorProduct vendorProduct),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (createVendorProductClicked != null) {
      return createVendorProductClicked(vendorProduct);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result createVendorProductClicked(_CreateVendorProductClicked value),
  }) {
    assert(createVendorProductClicked != null);
    return createVendorProductClicked(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result createVendorProductClicked(_CreateVendorProductClicked value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (createVendorProductClicked != null) {
      return createVendorProductClicked(this);
    }
    return orElse();
  }
}

abstract class _CreateVendorProductClicked implements VendorProductFormEvent {
  const factory _CreateVendorProductClicked(
      @required VendorProduct vendorProduct) = _$_CreateVendorProductClicked;

  @override
  VendorProduct get vendorProduct;
  @override
  _$CreateVendorProductClickedCopyWith<_CreateVendorProductClicked>
      get copyWith;
}

class _$VendorProductFormStateTearOff {
  const _$VendorProductFormStateTearOff();

// ignore: unused_element
  _VendorProductFormState call(
      {@required String name,
      @required String description,
      @required double price,
      @required List<String> images}) {
    return _VendorProductFormState(
      name: name,
      description: description,
      price: price,
      images: images,
    );
  }
}

// ignore: unused_element
const $VendorProductFormState = _$VendorProductFormStateTearOff();

mixin _$VendorProductFormState {
  String get name;
  String get description;
  double get price;
  List<String> get images;

  $VendorProductFormStateCopyWith<VendorProductFormState> get copyWith;
}

abstract class $VendorProductFormStateCopyWith<$Res> {
  factory $VendorProductFormStateCopyWith(VendorProductFormState value,
          $Res Function(VendorProductFormState) then) =
      _$VendorProductFormStateCopyWithImpl<$Res>;
  $Res call(
      {String name, String description, double price, List<String> images});
}

class _$VendorProductFormStateCopyWithImpl<$Res>
    implements $VendorProductFormStateCopyWith<$Res> {
  _$VendorProductFormStateCopyWithImpl(this._value, this._then);

  final VendorProductFormState _value;
  // ignore: unused_field
  final $Res Function(VendorProductFormState) _then;

  @override
  $Res call({
    Object name = freezed,
    Object description = freezed,
    Object price = freezed,
    Object images = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed ? _value.name : name as String,
      description:
          description == freezed ? _value.description : description as String,
      price: price == freezed ? _value.price : price as double,
      images: images == freezed ? _value.images : images as List<String>,
    ));
  }
}

abstract class _$VendorProductFormStateCopyWith<$Res>
    implements $VendorProductFormStateCopyWith<$Res> {
  factory _$VendorProductFormStateCopyWith(_VendorProductFormState value,
          $Res Function(_VendorProductFormState) then) =
      __$VendorProductFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {String name, String description, double price, List<String> images});
}

class __$VendorProductFormStateCopyWithImpl<$Res>
    extends _$VendorProductFormStateCopyWithImpl<$Res>
    implements _$VendorProductFormStateCopyWith<$Res> {
  __$VendorProductFormStateCopyWithImpl(_VendorProductFormState _value,
      $Res Function(_VendorProductFormState) _then)
      : super(_value, (v) => _then(v as _VendorProductFormState));

  @override
  _VendorProductFormState get _value => super._value as _VendorProductFormState;

  @override
  $Res call({
    Object name = freezed,
    Object description = freezed,
    Object price = freezed,
    Object images = freezed,
  }) {
    return _then(_VendorProductFormState(
      name: name == freezed ? _value.name : name as String,
      description:
          description == freezed ? _value.description : description as String,
      price: price == freezed ? _value.price : price as double,
      images: images == freezed ? _value.images : images as List<String>,
    ));
  }
}

class _$_VendorProductFormState implements _VendorProductFormState {
  const _$_VendorProductFormState(
      {@required this.name,
      @required this.description,
      @required this.price,
      @required this.images})
      : assert(name != null),
        assert(description != null),
        assert(price != null),
        assert(images != null);

  @override
  final String name;
  @override
  final String description;
  @override
  final double price;
  @override
  final List<String> images;

  @override
  String toString() {
    return 'VendorProductFormState(name: $name, description: $description, price: $price, images: $images)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _VendorProductFormState &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.price, price) ||
                const DeepCollectionEquality().equals(other.price, price)) &&
            (identical(other.images, images) ||
                const DeepCollectionEquality().equals(other.images, images)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(price) ^
      const DeepCollectionEquality().hash(images);

  @override
  _$VendorProductFormStateCopyWith<_VendorProductFormState> get copyWith =>
      __$VendorProductFormStateCopyWithImpl<_VendorProductFormState>(
          this, _$identity);
}

abstract class _VendorProductFormState implements VendorProductFormState {
  const factory _VendorProductFormState(
      {@required String name,
      @required String description,
      @required double price,
      @required List<String> images}) = _$_VendorProductFormState;

  @override
  String get name;
  @override
  String get description;
  @override
  double get price;
  @override
  List<String> get images;
  @override
  _$VendorProductFormStateCopyWith<_VendorProductFormState> get copyWith;
}
