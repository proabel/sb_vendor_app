part of 'vendor_product_form_bloc.dart';

@freezed
abstract class VendorProductFormEvent with _$VendorProductFormEvent {
  const factory VendorProductFormEvent.createVendorProductClicked(
    @required VendorProduct vendorProduct) = _CreateVendorProductClicked;
}