part of 'vendor_product_form_bloc.dart';



@freezed
abstract class VendorProductFormState with _$VendorProductFormState {
  const factory VendorProductFormState({
    @required String name,
    @required String description,
    @required double price,
    @required List<String> images 
  }) = _VendorProductFormState;

  factory VendorProductFormState.initial() => VendorProductFormState(
    name: '',
    description: '',
    price: 0,
    images: []
  );
}

