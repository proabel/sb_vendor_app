// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:sb_vendor/infrastructure/core/firebase_injectable_module.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sb_vendor/infrastructure/auth/auth_repository.dart';
import 'package:sb_vendor/domain/auth/i_auth_repository.dart';
import 'package:sb_vendor/infrastructure/selection/selection_repository.dart';
import 'package:sb_vendor/domain/selection/i_selection_repository.dart';
import 'package:sb_vendor/infrastructure/vendor_product/vendor_product_repository.dart';
import 'package:sb_vendor/domain/vendor_product/i_vendor_product_repository.dart';
import 'package:sb_vendor/application/selection/selection_bloc.dart';
import 'package:sb_vendor/application/auth/sign_in_form/sign_in_form_bloc.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  final firebaseInjectableModule = _$FirebaseInjectableModule();
  g.registerLazySingleton<Firestore>(() => firebaseInjectableModule.firestore);
  g.registerLazySingleton<IAuthRepository>(() => AuthRepository());
  g.registerLazySingleton<ISelectionRepository>(
      () => SelectionRepository(g<Firestore>()));
  g.registerLazySingleton<IVendorProductRepository>(
      () => VendorProductRepository(g<Firestore>()));
  g.registerFactory<SelectionBloc>(
      () => SelectionBloc(g<ISelectionRepository>()));
  g.registerFactory<SignInFormBloc>(() => SignInFormBloc(g<IAuthRepository>()));
}

class _$FirebaseInjectableModule extends FirebaseInjectableModule {}
